#include "Strand.h"
#include "BondSegment.h"
#include "Segment.h"

using namespace std;

Strand::Strand(seg_ptr init, bool cyclic) : len(0), cyclic(cyclic), segments(0), links(0), 
	last_ns_evt(0),
	ns(0)
	 {
	if(init) {
#ifndef PTR32
		segments = init;
		segments->next = cyclic ? init : 0;
		len += init->length();
#else
		assert(!init);
#endif
	}
}

Strand::~Strand() {
	segments = 0;

}

bool Strand::isCyclic() const {
	return cyclic;
}

uint32_t Strand::length() const {
	return len;
}

std::ostream& operator<<(std::ostream& os, Strand& st) {
	os << "Strand " << &st << " (len=" << st.len;
	if(st.cyclic)
		os << ", cyclic";
	if(!st.ns)
		os << ", OLD/DETACHED";
	if(st.ns && st.ns->eol)
		os << ", DEAD";
	os << ")" << std::endl;

	seg_ptr seg = st.segments;
	if(seg)
		do {
			Segment* ss;
			getSeg(seg, ss);
			os << *ss;
			seg = ss->next;
		} while(seg && seg != st.segments);
	else
		os << "INVALID STRAND";
	os << std::endl;
#ifndef PTR32
	ls_ptr_use ls = st.links;
	if(ls)
		do {
			ls->print(os, &st);
			ls = ls->context(&st).next;
		} while(ls);
#else
	os << "[STRAND LINKS OUTPUT DISABLED DUE TO PTR32]" << std::endl;
#endif
	os << std::endl;
	return os;
}


NS_info::NS_info(s_ptr_store parent, int32_t last_pos, seg_ptr curr_segment, int32_t seg_pos, int32_t t)
	: last_pos(last_pos), last_time(0), next_time(t), next_pos((last_pos + 1)), seg_pos(seg_pos), event_time(t),
	eol(false), parent(parent), curr_segment(curr_segment) 
{
}
