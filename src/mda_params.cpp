#include <cassert>
#include <algorithm>

#include "mda_params.h"

using namespace std;

//TODO: fixme
mda_params::mda_params() {
	// P_err
	p_err[0][0] = 0.99997; p_err[0][1] = 0.00001; p_err[0][2] = 0.00001; p_err[0][3] = 0.00001;
	p_err[1][0] = 0.99997; p_err[1][1] = 0.00001; p_err[1][2] = 0.00001; p_err[1][3] = 0.00001;
	p_err[2][0] = 0.99997; p_err[2][1] = 0.00001; p_err[2][2] = 0.00001; p_err[2][3] = 0.00001;
	p_err[3][0] = 0.99997; p_err[3][1] = 0.00001; p_err[3][2] = 0.00001; p_err[3][3] = 0.00001;
	// P_eol
	
	// OK:
	p_eol[0][0][0] = 1.0;    p_eol[0][0][1] = 0.0001; p_eol[0][0][2] = 1.0;    p_eol[0][0][3] = 1.0;
	p_eol[0][1][0] = 0.0001; p_eol[0][1][1] = 1.0;    p_eol[0][1][2] = 1.0;    p_eol[0][1][3] = 1.0;
	p_eol[0][2][0] = 1.0;    p_eol[0][2][1] = 1.0;    p_eol[0][2][2] = 1.0;    p_eol[0][2][3] = 0.0001;
	p_eol[0][3][0] = 1.0;    p_eol[0][3][1] = 1.0;    p_eol[0][3][2] = 0.0001; p_eol[0][3][3] = 1.0;
	// REPLACE
	p_eol[1][0][0] = 0.5;    p_eol[1][0][1] = 0.5;    p_eol[1][0][2] = 0.5;    p_eol[1][0][3] = 0.5;
	p_eol[1][1][0] = 0.5;    p_eol[1][1][1] = 0.5;    p_eol[1][1][2] = 0.5;    p_eol[1][1][3] = 0.5;
	p_eol[1][2][0] = 0.5;    p_eol[1][2][1] = 0.5;    p_eol[1][2][2] = 0.5;    p_eol[1][2][3] = 0.5;
	p_eol[1][3][0] = 0.5;    p_eol[1][3][1] = 0.5;    p_eol[1][3][2] = 0.5;    p_eol[1][3][3] = 0.5;
	// SKIP
	p_eol[2][0][0] = 0.5;    p_eol[2][0][1] = 0.5;    p_eol[2][0][2] = 0.5;    p_eol[2][0][3] = 0.5;
	p_eol[2][1][0] = 0.5;    p_eol[2][1][1] = 0.5;    p_eol[2][1][2] = 0.5;    p_eol[2][1][3] = 0.5;
	p_eol[2][2][0] = 0.5;    p_eol[2][2][1] = 0.5;    p_eol[2][2][2] = 0.5;    p_eol[2][2][3] = 0.5;
	p_eol[2][3][0] = 0.5;    p_eol[2][3][1] = 0.5;    p_eol[2][3][2] = 0.5;    p_eol[2][3][3] = 0.5;
	// ADD
	p_eol[3][0][0] = 0.5;    p_eol[3][0][1] = 0.5;    p_eol[3][0][2] = 0.5;    p_eol[3][0][3] = 0.5;
	p_eol[3][1][0] = 0.5;    p_eol[3][1][1] = 0.5;    p_eol[3][1][2] = 0.5;    p_eol[3][1][3] = 0.5;
	p_eol[3][2][0] = 0.5;    p_eol[3][2][1] = 0.5;    p_eol[3][2][2] = 0.5;    p_eol[3][2][3] = 0.5;
	p_eol[3][3][0] = 0.5;    p_eol[3][3][1] = 0.5;    p_eol[3][3][2] = 0.5;    p_eol[3][3][3] = 0.5;
	
	// P_add
	p_add[0][0] = 0.25; p_add[0][1] = 0.25; p_add[0][2] = 0.25; p_add[0][3] = 0.25;
	p_add[1][0] = 0.25; p_add[1][1] = 0.25; p_add[1][2] = 0.25; p_add[1][3] = 0.25;
	p_add[2][0] = 0.25; p_add[2][1] = 0.25; p_add[2][2] = 0.25; p_add[2][3] = 0.25;
	p_add[3][0] = 0.25; p_add[3][1] = 0.25; p_add[3][2] = 0.25; p_add[3][3] = 0.25;

	// P_replace
	p_replace[0][0] = 0.3333; p_replace[0][1] = 0.0000; p_replace[0][2] = 0.3333; p_replace[0][3] = 0.3334;
	p_replace[1][0] = 0.0000; p_replace[1][1] = 0.3333; p_replace[1][2] = 0.3334; p_replace[1][3] = 0.3333;
	p_replace[2][0] = 0.3333; p_replace[2][1] = 0.3334; p_replace[2][2] = 0.3333; p_replace[2][3] = 0.0000;
	p_replace[3][0] = 0.3334; p_replace[3][1] = 0.3333; p_replace[3][2] = 0.0000; p_replace[3][3] = 0.3333;

	primer_len = 6;
	primer_n = 1000;
}


//TODO: add eol check code
bool mda_params::checkParams() {
#ifndef NDEBUG
	double s_err[4] = {0},  s_add[4] = {0}, s_replace[4] = {0};
	double tol = 1e-6;
	for(size_t i = 0; i < 4; ++i)
		for(size_t j = 0; j < 4; ++j) {
			s_err[i] += p_err[i][j];
			s_add[i] += p_add[i][j];
			s_replace[i] += p_replace[i][j];
		}
	for(size_t i = 0; i < 4; ++i) {
		assert(std::abs(s_err[i] - 1) < tol);
		assert(std::abs(s_add[i] - 1) < tol);
		assert(std::abs(s_replace[i] - 1) < tol);
		assert(p_replace[i][(i & ~1) + 1 - (i % 2)] == 0);
	}
#endif
	return true;
}
