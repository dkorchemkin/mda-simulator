#include "Segment.h"
#include "utils.h"
using namespace std;

#ifdef ONE_DNA_STRING
DNAString *dna_string = 0;
#endif


std::ostream& operator<<(std::ostream& os, const Segment& s) {
	s.print(os);
	return os;
}

StringSegment::StringSegment(DNAString* const & dna_string_, const uint32_t& begin, const uint32_t& end, const uint32_t& complementary):
#ifndef ONE_DNA_STRING
	dna_string(dna_string_), 
#endif
	comp(complementary), begin(begin) {
		len_ = end + 1 - begin;
		dna_string->cnt_t[comp][begin]++;
		dna_string->cnt_t[comp][end+1]--;
		base = &((*dna_string)[comp][begin]);
	}
void StringSegment::print(std::ostream& os) const {
	const std::string& cs = (*dna_string)[comp];
	os << "[SS: ";
	for(size_t i = begin; i < begin + len_; ++i)
		os << cs[i % cs.size()];
	os << ", " << dna_string << "[" << comp << "]{" << begin << "-" << begin + len_ - 1 << "}";
	os << "]";
}

Segment* StringSegment::complementary_str(uint32_t pos) const {
	uint32_t end = dna_string->reverseIdx((begin + pos) % dna_string->length());
	return new StringSegment(dna_string, end, end, 1 - comp);
}

void StringSegment::complementary_str(uint32_t pos, StringSegment* ss) const {
	uint32_t end = dna_string->reverseIdx((begin + pos) % dna_string->length());
	new (ss) StringSegment(dna_string, end, end, 1 - comp);
}

ErrorSegment::ErrorSegment(const char& error) : Segment(false) {
#ifndef NDEBUG
	assert(utils::check(error));
#endif
	base = (char*)&len_;
	const_cast<char&>(base[0]) = error;
}
void ErrorSegment::print(std::ostream& os) const {
	os << "[ES: " << base[0] << "]";
}

Segment* ErrorSegment::complementary() const {
	return new ErrorSegment(utils::complementary(base[0]));
}
void ErrorSegment::complementary(ErrorSegment* es) const {
	new (es) ErrorSegment(utils::complementary(base[0]));
}

void StringSegment::elongate(uint32_t cnt) {
	dna_string->cnt_t[comp][begin]--;
	assert(begin >= cnt);
	begin = (dna_string->length() + begin - cnt) % dna_string->length();
	dna_string->cnt_t[comp][begin]++;
	len_ += cnt;
}

void StringSegment::elongate_reverse(uint32_t cnt) {
	dna_string->cnt_t[comp][begin + len_]++;
	len_ += cnt;
	dna_string->cnt_t[comp][begin + len_]--;
	assert(begin + len_ <= dna_string->length());
}
