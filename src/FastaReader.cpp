#include <vector>
#include <utility>
#include <locale>

#include "FastaReader.h"

FastaReader::FastaReader(const char* filename) : in(filename) {
}

std::string FastaReader::read() {
	std::string temp, res;
	std::vector<std::string> strings;
	size_t total = 0;
	
	while(!in.eof()) {
		getline(in, temp);
		if(temp[0] == '>' || temp[0] == ';')
			continue;
		total += temp.size();
		strings.push_back(std::move(temp));
	}
	res.reserve(total);

	std::locale loc;

	for(auto s: strings)
		for(auto c: s)
			if(c == 'A' || c == 'T' || c == 'G' || c == 'C' || c == 'a' || c == 't' || c == 'g' || c == 'c')
				res.push_back(std::toupper(c, loc));
	return res;
}
