#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cstring>
#include <malloc.h>

#include "randutils.h"
#include "MDAModel.h"
#include "FastaReader.h"


using namespace std;

string src_dna;
mda_params params;
bool reusePrimers, scalePrimers, selectFromAll;
string prefix;
ostream* o_logp = &cout;
MDAModel * m;

void skipComments(ifstream& is) {
	char peek = is.peek();
	while(peek == '#' || isspace(peek) || peek == '\r' || peek == '\n') {
		if(peek == '#') {
			string s;
			getline(is, s);
		} else
			is.get();
		peek = is.peek();
	}
}

#define READ(a, is) { skipComments(is); is >> a; /*cout << a << endl;*/}

void setup(const string& filename) {
	int dummy;
	ifstream is(filename);
	skipComments(is);
	for(int i = 0; i < 4; ++i)
		for(int j = 0; j < 4; ++j)
			READ(params.p_err[i][j], is);
	for(int k = 0; k < 4; ++k)
		for(int i = 0; i < 4; ++i)
			for(int j = 0; j < 4; ++j)
				READ(params.p_eol[k][i][j], is);
	for(int i = 0; i < 4; ++i)
		for(int j = 0; j < 4; ++j)
			READ(params.p_add[i][j], is);
	for(int i = 0; i < 4; ++i)
		for(int j = 0; j < 4; ++j)
			READ(params.p_replace[i][j], is);
	READ(params.primer_len, is);
	READ(dummy, is);
	READ(params.primer_n, is);
	string s;
	skipComments(is);
	getline(is, s);
	//cout << s << endl;
	params.reusePrimers = strcmp(s.c_str(), "reuse") == 0;
	skipComments(is);
	getline(is, s);
	//cout << s << endl;
	params.scalePrimers = strcmp(s.c_str(), "scale") == 0;
	skipComments(is);
	getline(is, s);
	//cout << s << endl;
	if(strcmp(s.c_str(), "off") == 0)
		params.limit_type = LimitType::Off;
	if(strcmp(s.c_str(), "fixed") == 0) {
		params.limit_type = LimitType::Fixed;
		READ(params.pos_limit, is);
	}
	if(strcmp(s.c_str(), "normal") == 0) {
		params.limit_type = LimitType::Normal;
		READ(params.pos_limit_m, is);
		READ(params.pos_limit_sigma, is);
	}
	skipComments(is);
	getline(is, prefix);
	//cout << prefix << endl;
	if(strcmp(prefix.c_str(), "cout") != 0) {
		o_logp = new ofstream(prefix + ".log");
	}
	//cout << prefix << endl;
	READ(s, is);
	FastaReader fs(s.c_str());
	src_dna = fs.read();
}

void writeParams() {
	char errs[] = {'O', 'R', 'S', 'A'};
	char chars[] = {'A', 'T', 'G', 'C'};
	ostream& o_log = *o_logp;
	o_log << "Probs:" << endl << "Prob_err" << endl << "\t";
	for(int i = 0; i < 4; ++i)
		o_log << errs[i] << "\t";
	o_log << endl;
	for(int i = 0; i < 4; ++i) {
		o_log << chars[i] << "\t";
		for(int j = 0; j < 4; ++j)
			o_log << params.p_err[i][j] << "\t";
		o_log << endl;
	}
	o_log << endl << "Prob_eol" << endl << "\t";
	for(int i = 0; i < 4; ++i)
		o_log << errs[i] << "\t";
	o_log << endl;
	for(int i = 0; i < 4; ++i) {
		o_log << chars[i] << "\t";
		for(int j = 0; j < 4; ++j)
			o_log << params.p_eol[i][j] << "\t";
		o_log << endl;
	}
	o_log << endl << "Prob_add" << endl << "\t";
	for(int i = 0; i < 4; ++i)
		o_log << chars[i] << "\t";
	o_log << endl;
	for(int i = 0; i < 4; ++i) {
		o_log << chars[i] << "\t";
		for(int j = 0; j < 4; ++j)
			o_log << params.p_add[i][j] << "\t";
		o_log << endl;
	}
	o_log << endl << "Prob_rep" << endl << "\t";
	for(int i = 0; i < 4; ++i)
		o_log << chars[i] << "\t";
	o_log << endl;
	for(int i = 0; i < 4; ++i) {
		o_log << chars[i] << "\t";
		for(int j = 0; j < 4; ++j)
			o_log << params.p_replace[i][j] << "\t";
		o_log << endl;
	}
	o_log << endl << "Primer length: " << params.primer_len << endl;
	o_log << "Different primer number: " << params.primer_n << endl;
	o_log << "Primers reuse: " << (reusePrimers ? "T" : "F") << endl << endl;
	o_log << "Limit type: ";
	switch(params.limit_type) {
		case LimitType::Off:
			o_log << "Off";
			break;
		case LimitType::Fixed:
			o_log << "Fixed (" << params.pos_limit << ")";
			break;
		case LimitType::Normal:
			o_log << "Normal (" << params.pos_limit_m << ", " << params.pos_limit_sigma << ")";
			break;
	}
	o_log << endl;
	o_log << "SRC DNA size: " << src_dna.size() << endl << endl << endl;
}


void setupMDAModel() {
	m = new MDAModel(params, o_logp);
	m->addStrand(src_dna);
}

void simulate() {
	double cov = 0.0;
	int iteration = 0;
	ostream& o_log = *o_logp;
	while(cov < 1e9 && iteration < 3) {
		m->step();
		++iteration;
		stringstream ss;
		ss << prefix << "_coverage_" << iteration << ".csv";
		o_log << "Amplification: " << (cov = m->computeCoverage(ss.str())) << endl;
		if(false) {
			o_log << "No primers attached... finishing" << endl;
			break;
		}
//		malloc_info(0, stdout);
	}
}

int main(int argc, char** argv) {
	ios_base::sync_with_stdio(0);
	if (argc != 2) {
		cout << "First argument - job description" << endl;
		return 0;
	}
	setup(argv[1]);
	writeParams();
	setupMDAModel();
	simulate();
	o_logp->flush();
	delete o_logp;
	delete m;
	return 0;
}
