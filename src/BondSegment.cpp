#include <cassert>

#include "BondSegment.h"

using namespace std;

BondSegment::BondContext::BondContext(s_ptr_store other, std::uint32_t begin, std::uint32_t end, ls_ptr_store next, ls_ptr_store prev)
	: other(other), begin(begin), end(end), next(next)
	  , prev(prev) 
{
#ifndef NDEBUG
		assert(begin >= 0);
		assert(end >= 0);
#endif
	}
BondSegment::BondContext& BondSegment::context(s_ptr_store ctx) {
#ifdef CHECK
	assert(ctx == A.other || ctx == B.other);
#endif
	return A.other == ctx ? B : A;
}

std::uint32_t BondSegment::length() {
#ifdef CHECK
	assert(A.end + 1 - A.begin == B.end + 1 - B.begin);
#endif
	return A.end + 1 - A.begin;
}

void BondSegment::print(std::ostream& os, Strand* ctx) {
#ifndef PTR32
	BondContext& this_ctx = context(ctx);
	BondContext& other_ctx = context(this_ctx.other);
	os << "[{" << this_ctx.other << ":{" << this_ctx.begin << ":" << this_ctx.end << "}-{" << other_ctx.end << ":" << other_ctx.begin << "}]";
#else
	os << "[LINK OUTPUT DISABLED DUE TO PTR32]";
#endif
}

BondSegment::BondSegment(s_ptr_store A, s_ptr_store B, std::uint32_t begin_A, std::uint32_t end_A, std::uint32_t begin_B, std::uint32_t end_B) : 
	A(B, begin_A, end_A), B(A, begin_B, end_B)
{
#ifdef CHECK
		assert(this->A.end + 1 - this->A.begin == this->B.end + 1 - this->B.begin);
#endif
}


void BondSegment::del(ls_ptr_store idx) {
	ls_ptr_use AP = A.prev, AN = A.next;
	ls_ptr_use this_ = idx;
	s_ptr_use AS = B.other;
	if(AP)
		AP->context(AS).next = AN;
	if(AN)
		AN->context(AS).prev = AP;
	if(AS->links == this_) {
		AS->links = AN;
	}

	ls_ptr_use BP = B.prev, BN = B.next;
	s_ptr_use BS = A.other;
	if(BP)
		BP->context(BS).next = BN;
	if(BN)
		BN->context(BS).prev = BP;
	if(BS->links == this_) {
		BS->links = BN;
	}

}
