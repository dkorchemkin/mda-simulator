#include <iostream>
#include <fstream>
#include <map>
#include <algorithm>
#include <cassert>
#include <cstdint>
#include <limits.h>
#include <numeric> 
#include <set>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <malloc.h>
#include <omp.h>

#include "MDAModel.h"
#include "utils.h"
#include "Segment.h"
#include "randutils.h"
#include "BinaryMatcher.h"
#include "StreamSelector.h"

MDAModel::MDAModel(mda_params params, std::ostream* logp) : 
	reusePrimers(params.reusePrimers), scalePrimers(params.scalePrimers), 
	params(params), output(0), debug(0),
	o_log_p(logp), rngs((Rng*) operator new[] (sizeof(Rng) *threads)),
#ifndef PTR32
	ls_allocator(1),
#endif
	urngs(new std::uniform_real_distribution<double>[threads])
{
	nss_mem = nullptr;
	std::random_device rng_d;
	size_t seed0 = rng_d();
	//seed0 = 3108617051LLU;
	std::mt19937_64 seed_rng(seed0);
	std::cout << "SEED: " << seed0 << std::endl;
	for(size_t i = 0; i < threads; ++i)
		new (rngs + i) Rng(seed_rng());
	}

MDAModel::~MDAModel() {
	for(auto it = strings.begin(); it != strings.end(); ++it)
		delete (*it);
	for(size_t i = 0; i < threads; ++i)
		rngs[i].~Rng();
	delete[] rngs;
	delete[] urngs;
}

void MDAModel::addStrand(const std::string& s, bool cyclic) {
	string ss(s);
	std::reverse(ss.begin(), ss.end());
	DNAString* init = new DNAString(ss, params);

#ifdef ONE_DNA_STRING
	assert(!dna_string);
	dna_string = init;
#endif

	strings.push_back(init);
	std::uint32_t idx;
	Strand* str_ptr = s_allocator.allocateNew(0, idx);
	new (str_ptr) Strand(0, cyclic);

#ifdef PTR32
	s_ptr_use str(idx);
	Segment* last_ptr = 0;
	seg_ptr sseg_ptr;

	StringSegment* sseg = ss_allocator.allocateNew(0, sseg_ptr);
	new (sseg) StringSegment(init, 0, (uint32_t)ss.size() - 1, 0);

	str->segments = sseg_ptr;
	last_ptr = sseg;
	str->len += Segment::length(sseg_ptr);

	if (cyclic) {
		last_ptr->next = str->segments;
		Segment* sssp;
		getSeg(str->segments, sssp);
		assert(sssp);
	}
	assert(Segment::length(str->segments) == s.size());
#else
	s_ptr_use str(str_ptr);
	Segment* last = 0;

	StringSegment *sseg = ss_allocator.allocateNew(0);
	new (sseg) StringSegment(init, 0, (uint32_t)ss.size() - 1, 0);

	str->segments = sseg;
	last = sseg;
	str->len += sseg->length();

	if (cyclic) {
		last->next = str->segments;
	}
	assert(str->segments->length() == s.size());
#endif

	assert(str->len == s.size());
	assert(s.size());
	assert(str);
	assert(s.size());
	assert(str);
	strands.push_back(str);
}


void MDAModel::step() {
	std::ostream& o_log = *o_log_p;
	
	o_log << "Primers generation" << std::endl;
	timer.startTick();
	generatePrimers();
	timer.stopTick("Primers generation");
	
	o_log << "Primers search" << std::endl;
	timer.startTick();
	findPrimers();
	timer.stopTick("Primers search");

	o_log << "Strands creation" << std::endl;
	timer.startTick();

	size_t sss = strands.size();
	omp_set_num_threads(threads);
	size_t max_len = 0, min_len = ~0llu, avg_len = 0, cnt = 0;

#pragma omp parallel for reduction(max:max_len) reduction(min:min_len) reduction(+:avg_len,cnt) schedule(dynamic, 10000)
	for(size_t i = 0; i < sss; ++i) {
		if(nss_cnt[i])
			processStrand(i, omp_get_thread_num(), max_len, min_len, avg_len, cnt);
	}

	ns_allocator.printStats();
	ss_allocator.printStats();
	es_allocator.printStats();
	s_allocator.printStats();
	ls_allocator.printStats();
	
	ns_allocator.free();
	ls_allocator.shrink();
	
	cout << "MAX LEN: " << max_len << endl << "MIN LEN: " << min_len << endl << "AVG LEN: " << (double)avg_len / cnt << endl << "CNT: " << cnt << endl;
	
	timer.stopTick("Simulation");
	timer.printReport(o_log);
	timer.rewind();
	o_log.flush();
	
	steps++;
	
	delete[] nss_mem;
	nss_mem = nullptr;
	malloc_trim(0);
}

void MDAModel::processStrand(size_t strand, size_t thread, size_t &max_len, size_t &min_len, size_t &avg_len, size_t &cnt) {
	std::priority_queue<Evt> *events = nullptr;
	std::deque<s_ptr_store> ns;
	
	removeBadPrimers(strand, thread);
	prepareNS(strand, thread, events, ns);
	
	if(ns.size()) {
		simulateNS(strand, thread, events);
		#pragma omp critical
		{
		 	computeBonds(strand, thread);
			strands.insert(strands.end(), ns.begin(), ns.end());
			computeStrands(strand, thread, ns);
		}
		finalizeStrands(strand, thread, ns, max_len, min_len, avg_len, cnt);
	}
	
	ns_allocator.clear(thread);
}

void MDAModel::computeBonds(size_t strand, size_t thread){
	s_ptr_use it = strands[strand];
	s_ptr_use base = it->last_ns_evt;

	if (!base)
		return;
	
	vector<uint32_t> beg, end;
	
	prepareDeleteSet(beg, end, it);
	deleteSet(beg, end, it);
	
	ls_ptr_use new_links;
	
	prepareNewBonds(it, new_links);
	mergeBonds(it, new_links);
	
	it->last_ns_evt = 0;
}

void MDAModel::finalizeStrands(size_t strand, size_t thread, std::deque<s_ptr_store> &ns, size_t &max_len, size_t &min_len, size_t &avg_len, size_t &cnt) {
	// delete NSes
	// delete HSes
	// validate ns's bounds
	for(auto it : ns) {
		if(it->len > max_len) max_len = it->len;
		if(it->len < min_len) min_len = it->len;
		avg_len += it->len;
		cnt++;
	}
	ns.clear();
}

void MDAModel::computeStrands(size_t strand, size_t thread, std::deque<s_ptr_store> &ns) {
#ifdef PTR32
	uint64_t total_len = 0, maxl = 0, minl = -1;
	for (auto it : ns) {
		Segment* seg = 0;
		seg_ptr segp = 0;
		StringSegment* ss;
		ErrorSegment* es;
		for (auto s : it->ns->strand_events) {
			if (s.evt_type == SEvent::SKIP || s.evt_type == SEvent::EOL)
				continue;
			(void*&)ss = es = nullptr;
			Segment* sssp;
			bool isSS =getSeg(s.src, sssp);
			if(isSS) ss = static_cast<StringSegment*>(sssp);
			else es = static_cast<ErrorSegment*>(sssp);

			Segment* cs;
			seg_ptr idx;
			if (ss) {
				StringSegment* ns = ss_allocator.allocateNew(thread, idx);
				ss->complementary_str(s.end, ns);
				ns->elongate_reverse(s.end - s.begin);
				cs = ns;
			} else {
				ErrorSegment* ns = es_allocator.allocateNew(thread, idx);
				if(es)
					es->complementary(ns);
				else
					new (ns) ErrorSegment(s.c);
				cs = ns;
			}
			if (seg) {
				cs->next = segp;
			} else {
				cs->next = 0;
			}
			segp = idx;
			uint32_t slen = Segment::length(idx);
			it->len += slen;
			total_len += slen;
			seg = cs;
		}
		if(it->len < minl) minl = it->len;
		if (it->len > maxl) maxl = it->len;
		it->segments = segp;
		it->ns = 0;
	
	}
#else
	uint64_t total_len = 0, maxl = 0, minl = -1;
	for (auto it : ns) {
		Segment* seg = 0;
		Segment* sptr = 0;
		StringSegment* ss;
		ErrorSegment* es;
		for (auto s : it->ns->strand_events) {
			if (s.evt_type == SEvent::SKIP || s.evt_type == SEvent::EOL)
				continue;
			(void*&)ss = es = nullptr;
			if(s.src && s.src->ss)
				ss = static_cast<StringSegment*>(s.src);
			else
				es = static_cast<ErrorSegment*>(s.src);

			Segment* cs;
			seg_ptr idx;
			if (ss) {
				StringSegment* ns = ss_allocator.allocateNew(thread);
				ss->complementary_str(s.end, ns);
				ns->elongate_reverse(s.end - s.begin);
				cs = ns;
			} else {
				ErrorSegment* ns = es_allocator.allocateNew(thread);
				if(es)
					es->complementary(ns);
				else
					new (ns) ErrorSegment(s.c);
				cs = ns;
			}
			if (seg) {
				cs->next = seg;
			} else {
				cs->next = 0;
			}
			sptr = cs;
			it->len += cs->length();
			total_len += cs->length();
			seg = cs;
		}
		if(it->len < minl) minl = it->len;
		if (it->len > maxl) maxl = it->len;
		it->segments = sptr;
		it->ns = 0;
	
	}
#endif
}

void MDAModel::prepareNS(size_t strand, size_t thread, std::priority_queue<Evt> *&events, std::deque<s_ptr_store> &ns) {
	if(nss_cnt[strand])  {
		assert(!events);
		events = new std::priority_queue<Evt>;
		strands[strand]->last_ns_evt = 0;

		s_ptr_use sp = strands[strand];
		Strand& s = *sp;
		seg_ptr curr_seg = s.segments;
		uint32_t pos = 0, spos = 0, sdiff, pl = params.primer_len, len;
		s_ptr_use first = 0, last = 0;
		// now - attach all primers selected:
		for(size_t ite = 0; ite < nss_cnt[strand]; ++ite) {
			uint32_t* it = strand_pos[strand] + ite;
			ptrdiff_t begin = *it;
			// if is covered by new strands - skip this position
			// update segment
			sdiff = begin - pos;
#ifndef PTR32
			if(spos >= curr_seg->length()) {
				curr_seg = curr_seg->next;
#else
			if(spos>= Segment::length(curr_seg)) {
				Segment* ptr;
				getSeg(curr_seg, ptr);
				assert(ptr);
				curr_seg = ptr->next;
#endif
				spos = 0;
			}
			assert(curr_seg);
#ifndef PTR32
			while(spos + sdiff >= curr_seg->length()) {
#else
			uint32_t cslen = Segment::length(curr_seg);
			while(spos + sdiff >= cslen) {
#endif
				if(!spos) {
#ifndef PTR32
					sdiff -= curr_seg->length();
					curr_seg = curr_seg->next;
#else
					sdiff -= cslen;
					Segment* ptr;
					getSeg(curr_seg, ptr);
					assert(ptr);
					curr_seg = ptr->next;
					cslen = Segment::length(curr_seg);
#endif
					continue;
				}
				sdiff += spos;
				spos = 0;
			}
			spos += sdiff;
			pos = begin + pl;


			// create strand && create strand events
			std::uint32_t idx;
			Strand *new_strand_ptr = s_allocator.allocateNew(thread, idx);
			new (new_strand_ptr) Strand();
#ifndef PTR32
			s_ptr_use new_strand(new_strand_ptr);
#else
			s_ptr_use new_strand(idx);
#endif
			uint32_t rem = pl, addc;
			uint32_t idxns;
			NS_info* ns_ptr = ns_allocator.allocateNew(thread, idxns);
			new (ns_ptr) NS_info(sp, (pos - 1) > sp->len ? (pos - 1) % sp->len : (pos - 1), curr_seg, spos);
#ifndef PTR32
			new_strand->ns = ns_ptr;
#else
			new_strand->ns = idxns;
			cslen = Segment::length(curr_seg);
#endif

			while(rem > 0) {
#ifndef PTR32
				addc = (spos + rem) >= curr_seg->length() ? curr_seg->length() - spos : rem;
#else
				addc = (spos + rem) >= cslen ? cslen - spos : rem;
#endif
				StrandEvent evt = {curr_seg, spos, (spos + addc - 1), (pos - 1 - (rem - addc)), SEvent::OK, 0};
				new_strand->ns->strand_events.push_back(evt);
				if(addc != rem) {
					spos = 0;
#ifndef PTR32
					curr_seg = curr_seg->next;
					assert(curr_seg);
#else
					Segment* spp;
					getSeg(curr_seg, spp);
					assert(spp);
					curr_seg = spp->next;
					cslen = Segment::length(curr_seg);
#endif
				} else
					spos += rem;
				rem -= addc;
			}
			sym_cnt += pl;
			cnt_ok += pl;
			new_strand->ns->curr_segment = curr_seg;
			new_strand->ns->seg_pos = spos;
			new_strand->ns->steps = pl;
			new_strand->ns->start_pos = begin;
			switch(params.limit_type) {
				case LimitType::Fixed:
					new_strand->ns->eol_limit = params.pos_limit;
					break;
				case LimitType::Normal: 
					len = nrand(rngs[0], params.pos_limit_m, params.pos_limit_sigma);
					if (len < params.primer_len)
						len = params.primer_len;
					new_strand->ns->eol_limit = len;
					break;
				case LimitType::Off:
					break;
			}

			if(last)
				last->ns->dist_next = pos - 1 - last->ns->last_pos;
			else
				last = new_strand;
			last->ns->next_new = new_strand;
			new_strand->ns->prev_new = last;
			last = new_strand;
			if(!first) {
				first = new_strand;
			}
			first->ns->prev_new = new_strand;
			new_strand->ns->next_new = first;
			new_strand->ns->dist_next = (pos - 1) >= sp->len ? first->ns->last_pos - ((pos - 1) % sp->len) : sp->len - pos + 1 + first->ns->last_pos;
			ns.push_back(new_strand);
			events->push(Evt(new_strand, new_strand->ns->next_time));
		}
	}
}

void MDAModel::simulateNS(size_t strand, size_t thread, std::priority_queue<Evt> *&events) {
#ifndef NDEBUG
	uint32_t levt = 0;
#endif
	//int lt = events.top().t;
	while (events->size()) {
		Evt curr = events->top(); events->pop();
#ifndef NDEBUG
		assert(curr.ns->ns);
		assert(levt <= curr.t);
		levt = curr.t;
#endif
		if (curr.ns->ns->eol && curr.type != EvtType::OVERTAKE)
			continue;
		if (curr.type == EvtType::OVERTAKE && curr.timestamp != curr.ns->ns->valid_overtake) {
			updateOvertakes(curr.ns, curr.t, events);
			continue;
		}
		Strand& s = *curr.ns;
		NS_info& ns = *s.ns;
		s_ptr_use sp = curr.ns, parent = ns.parent;
		ptrdiff_t time = curr.t;

		if (curr.type == EvtType::ASSERT) {
			s_ptr_use base = parent->last_ns_evt, curr, nxt;
			curr = base;
			do {
				nxt = curr->ns->next_new;
#ifndef NDEBUG
				int curr_pos = curr->ns->next_time == time ? curr->ns->next_pos : curr->ns->next_pos - (curr->ns->next_time - time) / curr->ns->event_time;
				int next_pos = nxt->ns->next_time == time ? nxt->ns->next_pos : nxt->ns->next_pos - (nxt->ns->next_time - time) / nxt->ns->event_time;
				assert(next_pos != curr_pos);
#endif
				curr = nxt;
			} while (curr != base);
			continue;
		}

#ifdef PTR32
		Segment* sssp;
		getSeg(ns.curr_segment, sssp);
		uint32_t nsslen = Segment::length(ns.curr_segment);
#endif


		uint32_t newpos = ns.next_pos;
		parent->last_ns_evt = sp;

		EvtType next_evt;

		bool eol = false;
		CopyResult res;
		StrandEvent sevt;
		int copied, next_time;

		if (curr.type != EvtType::OVERTAKE) {
			if (newpos >= ns.parent->length()) {
				newpos %= ns.parent->length();
			}
			assert(ns.curr_segment);
			bool eoled = false;
#ifndef PTR32
			while (ns.curr_segment->length() <= ns.seg_pos) {
				ns.seg_pos -= ns.curr_segment->length();
				if(!ns.curr_segment->next) {
					assert(time == 10);
					//EOL!
					//Primer was allocated at the end of non-cyclic strand
					eol = true;
					ns.eol = true;
					newpos -= 1;
					ns.steps -= 1;

					//insert new event
					sp->last_ns_evt = curr.ns;
					ns.next_pos--;//wpos + copied;
					ns.valid_overtake = -1;
					sp->ns->next_pos %= sp->ns->parent->len;
					assert(ns.next_pos == (ns.start_pos + ns.steps) % parent->len);
					updateOvertakes(sp, time, events);
					eoled = true;
					break;
				}
				ns.curr_segment = ns.curr_segment->next;
#else
				while (nsslen <= ns.seg_pos) {
					ns.seg_pos -= nsslen;
					if(!sssp->next) {
						assert(time == 10);
						//EOL!
						//Primer was allocated at the end of non-cyclic strand
						eol = true;
						ns.eol = true;
						newpos -= 1;
						ns.steps -= 1;

						//insert new event
						sp->last_ns_evt = curr.ns;
						ns.next_pos--;//wpos + copied;
						ns.valid_overtake = -1;
						sp->ns->next_pos %= sp->ns->parent->len;
						assert(ns.next_pos == (ns.start_pos + ns.steps) % parent->len);
						updateOvertakes(sp, time, events);
						eoled = true;
						break;
					}
					ns.curr_segment = sssp->next;
					getSeg(ns.curr_segment, sssp);
					nsslen = Segment::length(ns.curr_segment);
#endif
				}	
				if(eoled) continue;
#ifndef NDEBUG
				assert(!ns.strand_events.size()
						|| newpos == ns.strand_events.rbegin()->strand_pos
						|| newpos == (ns.strand_events.rbegin()->strand_pos + 1) % parent->len);
#endif
#ifndef PTR32
				char nc = (*ns.curr_segment)[ns.seg_pos];
#else
				Segment* nscsptr;
				getSeg(ns.curr_segment, nscsptr);
				char nc = (*nscsptr)[ns.seg_pos];
#endif
				int nci = nc;

				bool next_err = false;
				if (curr.type == EvtType::ALLOW_ALL) {

					res = modelAllowAll(sp, nci, sevt, ns.curr_segment, ns.seg_pos, time, newpos, eol, copied, next_err, thread);
					if (copied == 0)
						curr.type = EvtType::ALLOW_ERR;
					else {
						if(ns.strand_events.size()) {
							StrandEvent& last = *ns.strand_events.rbegin();
							if(last.evt_type == SEvent::OK && last.end + 1 == sevt.begin && last.src == sevt.src) {
								last.end = sevt.end;
								last.strand_pos = sevt.strand_pos;
							} else
							{
								ns.strand_events.push_back(sevt);
							}
						}
					}
				} else { //EvtType::ALLOW_ERR) 

					res = modelError(sp, nci, sevt, ns.curr_segment, ns.seg_pos, time, newpos, eol, copied, thread);
					if (res != CopyResult::OK) {
						eol = rtest(rngs[thread], params.p_eol[res][nci][static_cast<size_t>(sevt.c)]);
					}
					ns.strand_events.push_back(sevt);
				}
				next_evt = next_err ? EvtType::ALLOW_ERR : EvtType::ALLOW_ALL;
				next_time = time + (res == CopyResult::Add ? 1 : copied) * ns.event_time;
#ifndef NDEBUG
#endif
				ns.last_pos = newpos; ns.last_time = time;
#ifndef PTR32
				if (ns.steps >= ns.eol_limit || (ns.seg_pos + copied >= ns.curr_segment->length() && !ns.curr_segment->next) || eol) {
#else
					if (ns.steps >= ns.eol_limit || (ns.seg_pos + copied >= nsslen && !sssp->next) || eol) {
#endif
#ifndef NDEBUG
#ifndef PTR32
						assert(ns.steps >= ns.eol_limit  || eol || ns.seg_pos + copied == ns.curr_segment->length());
#else
						assert(ns.steps >= ns.eol_limit  || eol || ns.seg_pos + copied == nsslen);
#endif
#endif
						eol = true;
						ns.eol = true;
						newpos -= copied ? 1 : 0;
						ns.steps -= copied ? 1 : 0;
					}
					sym_cnt += res == Add ? 1 : (res == Skip ? 0 :  copied);
					//insert new event
					sp->last_ns_evt = curr.ns;
					ns.next_pos = newpos + copied;
					ns.seg_pos += copied;
					ns.steps += copied;
					if (!eol) {
						ns.next_time = next_time;
						events->push(Evt(sp, ns.next_time, 0, next_evt));
#ifndef NDEBUG
						assert((ns.next_pos == parent->length() && parent->isCyclic()) || ns.next_pos < parent->length());
#endif
					} else {
						ns.valid_overtake = -1;
						ns.next_time = next_time;
						ns.next_time = next_time - (copied ? ns.event_time : 0);//INT32_MAX;
						sp->ns->next_pos %= sp->ns->parent->len;
#ifndef NDEBUG
						assert(sp->ns->next_pos == (sp->ns->start_pos + sp->ns->steps) % sp->ns->parent->len);
#endif
					}
#ifndef NDEBUG
					assert(sp->ns->next_pos % sp->ns->parent->len == (sp->ns->start_pos + sp->ns->steps) % sp->ns->parent->len);
#endif
				} else {
					modelOvertake(sp, 0, sevt, 0, 0, time, newpos, eol, copied, thread);
				}

				updateOvertakes(sp, time, events);
			}

			delete events;
			events = 0;
		}

void MDAModel::consistencyCheck() {
	// TODO: provide new test code
}

CopyResult MDAModel::modelError(s_ptr_store sp, int nci, StrandEvent& sevt, seg_ptr curr_segment, uint32_t seg_pos, uint32_t time, uint32_t &newpos, bool &eol, int &copied, size_t thread) {
	CopyResult res = CopyResult::Invalid;
	double p = urand(rngs[thread]);
	double perr = params.p_error_copy(nci);
	double p_ok_eol = params.p_err[nci][CopyResult::OK] * (params.p_eol[CopyResult::OK][nci][nci ^ 1] / perr);
	double p_rep = params.p_err[nci][CopyResult::Replace] / perr;
	double p_add = params.p_err[nci][CopyResult::Add] / perr;
	double p_skip = params.p_err[nci][CopyResult::Skip] / perr;
#ifndef NDEBUG
	assert(abs(1.0 - (p_ok_eol + p_rep + p_add + p_skip)) < 1e-5);
#endif

	// OK + EOL

	if (p < p_ok_eol) {
		eol = true;
		StrandEvent s = { curr_segment, seg_pos, seg_pos, newpos, SEvent::OK, 0 };
		sevt = s;
		res = CopyResult::OK;
		copied = 1;
		cnt_ok++;
		copied_ok++;
	}
	p -= p_ok_eol;
	// REPLACE
	if (p >= 0.0 && p < p_rep) {
		char rep = rselect(rngs[thread], params.p_replace[nci], 4);
		StrandEvent s = { 0, seg_pos, seg_pos, newpos, SEvent::REPLACE, rep };
		sevt = s;
		res = CopyResult::Replace;
		copied = 1;
		cnt_rep++;
		copied_replace++;
	}
	p -= p_rep;
	// SKIP
	if (p >= 0.0 && p < p_skip) {
		StrandEvent s = { 0, seg_pos, seg_pos, newpos, SEvent::SKIP, 0 };
		sevt = s;
		res = CopyResult::Skip;
		copied = 1;
		copied_skip++;
	}
	p -= p_skip;
	// ADD
	if (p >= 0.0 && p < p_add) {
		char add = rselect(rngs[thread], params.p_add[nci], 4);
		StrandEvent s = { 0, seg_pos, seg_pos-1, newpos, SEvent::ADD, add };
		sevt = s;
		res = CopyResult::Add;
		copied = 0;
		cnt_add++;
		copied_add++;
	}
	p -= p_add;
#ifndef NDEBUG
	assert(p <= 0.0);
#endif
	return res;
}

CopyResult MDAModel::modelAllowAll(s_ptr_store strand, int char_index, StrandEvent& strand_event, seg_ptr curr_segment, uint32_t seg_pos, uint32_t time, uint32_t &new_pos, bool &eol, int &copied, bool &next_err, size_t thread) {
	double p = urand(rngs[thread]);
	uint32_t succ_copy;
	StringSegment* ss = nullptr;
#ifndef PTR32
	if(curr_segment->ss)
		ss = static_cast<StringSegment*>(curr_segment);
#else
	Segment* ssp;
	if(getSeg(curr_segment, ssp))
		ss = static_cast<StringSegment*>(ssp);
#endif
	bool next_ok = true;
	if (ss)
		succ_copy = (int)ss->succ_copy(seg_pos, p, next_ok);
	else
		succ_copy = p < params.p_err[char_index][CopyResult::OK] * (1.0 - params.p_eol[CopyResult::OK][char_index][char_index ^ 1]) ? 1 : 0;
	succ_copy = std::min(strand->ns->eol_limit - strand->ns->steps, succ_copy);
	copied = succ_copy;
	if(succ_copy == strand->ns->eol_limit - strand->ns->steps)
		copied++; 
	if (succ_copy != 0) {
		cnt_ok += copied;
		copied_ok += copied;
		next_err  = !next_ok;
		strand_event = { curr_segment, seg_pos, seg_pos + copied - 1, (new_pos + copied - 1), SEvent::OK, 0};
	}
	return CopyResult::OK;
}


void MDAModel::updateOvertakes(s_ptr_store A, int time, std::priority_queue<Evt>* events) {
#ifndef NDEBUG
	assert(A->ns && A->ns->prev_new && A->ns->next_new);
#endif
	s_ptr_use strand = A, next = A->ns->next_new, prev = A->ns->prev_new;
	uint32_t otTime = ~0u;
	int cnt = 0;
	if (calculateOvertake(strand, next, time, otTime)) {
		if (otTime != strand->ns->valid_overtake) {
#ifdef MEMORY_USAGE
			strand_ns_events++;
#endif
			events->push(Evt(strand, otTime, otTime, EvtType::OVERTAKE));
		}
		strand->ns->valid_overtake = otTime;
		cnt++;
	}
	if (calculateOvertake(prev, strand, time, otTime)) {
		if (otTime != prev->ns->valid_overtake) {
#ifdef MEMORY_USAGE
			strand_ns_events++;
#endif
			events->push(Evt(prev, otTime, otTime, EvtType::OVERTAKE));
		}
		prev->ns->valid_overtake = otTime;
		cnt++;
	}
#ifndef NDEBUG
	//TODO: CHECKME
#if 0
	if (-strand->ns->steps + strand->ns->dist_next + next->ns->steps < 0 && next->ns->next_time >= strand->ns->next_time)
		assert(cnt);
	if (-prev->ns->steps + prev->ns->dist_next + strand->ns->steps < 0 && prev->ns->next_time <= strand->ns->next_time)
		assert(cnt);
#endif
#endif
}

CopyResult MDAModel::modelOvertake(s_ptr_store strand, int char_index, StrandEvent& strand_event, seg_ptr curr_segment, uint32_t seg_pos, uint32_t time, uint32_t &new_pos, bool &eol, int &copied, size_t thread) {
	s_ptr_use A = strand, B = strand->ns->next_new;

#ifndef NDEBUG
	int dist_curr = distToNext(A, time);
	assert(dist_curr == 0 || dist_curr == -1);
#endif

	A->ns->next_new = B->ns->next_new;
	A->ns->dist_next += B->ns->dist_next;
	B->ns->next_new->ns->prev_new = A;
#ifndef NDEBUG
	assert(B->ns->next_pos == (B->ns->start_pos + B->ns->steps) % B->ns->parent->len);
#endif
	if (!B->ns->eol) {
		// b overtaken during burst or err => may need to adjust last pos, steps, next pos
		// only possible if different speeds.
		int B_update = stepsUpdate(B, time);
		B_update -= (B->ns->eol && B->ns->strand_events.rbegin()->evt_type != SEvent::ADD) ? 1 : 0;
		B->ns->steps += B_update;
		B->ns->next_pos += B_update + B->ns->parent->len;
		B->ns->next_pos %= B->ns->parent->len;

		//        int B_steps = (dtB + B->ns->event_time - 1) / B->ns->event_time;
		//      B->ns->steps -= B_steps;
		//    B->ns->last_pos = A->ns->last_pos;
		B->ns->strand_events.rbegin()->end += B_update ? B_update + 1 : 0;
		B->ns->strand_events.rbegin()->strand_pos += B_update ? B_update + 1 : 0;
		sym_cnt += B_update ? B_update + 1 : 0;
		cnt_ok += B_update ? B_update + 1 : 0;
#ifndef NDEBUG
		assert(B->ns->strand_events.rbegin()->strand_pos == B->ns->next_pos);
		assert(B->ns->next_pos == (B->ns->start_pos + B->ns->steps) % B->ns->parent->len);
#endif
		B->ns->eol = true;
	}
#ifndef NDEBUG
	assert(B->ns->next_pos == (B->ns->start_pos + B->ns->steps) % B->ns->parent->len);
#endif
	B->ns->eol = true;
	// B->ns->event_time = INT32_MAX;
	A->ns->valid_overtake = -1;
#ifndef NDEBUG
	assert(A->ns->last_time < time || A->ns->strand_events.rbegin()->strand_pos >= A->ns->next_pos + stepsUpdate(A, time));
	assert(A->ns->last_time == time || A->ns->strand_events.rbegin()->evt_type == SEvent::OK);
#endif
	A->ns->valid_overtake = -1;
	return CopyResult::OK;
}

void MDAModel::generatePrimers() {
	if(!scalePrimers) scale = 1;
	size_t total_primers = 1ULL << (2ULL * params.primer_len);
	primer_selection.clear();
	primer_selection.resize(total_primers);	
	double scale = this->scale * ((double)strands[0]->len / 5000000.0);

	size_t alloc;
	StreamSelector<size_t, std::mt19937_64, std::mt19937_64, true> ss(alloc, total_primers - 1, 1, rngs, urngs);
	for(size_t i = 1; i <= params.primer_n * scale + total_primers - 1; ++i) {
		size_t upd = ss.stepFastForward(i);
		i = i + upd - 1;
	}
	size_t ps;
	size_t *limits = const_cast<size_t *>(ss.fetchResult(ps)); // Dirty... need to change ptr in ss?
	sort(limits, limits + ps);
	size_t l = 1;
	for(size_t i = 0; i < ps; ++i) {
		primer_selection[i] = limits[i] - l;
		l = limits[i] + 1;
	}
	primer_selection[total_primers - 1] = params.primer_n * scale + total_primers - l;

	primers.clear();
	primers_selection_cnt.clear();
	for(size_t i = 0; i < total_primers; ++i) {
		if(!primer_selection[i])
			continue;
		std::string s;
		s.resize(params.primer_len);
		for(size_t j = 0; j < params.primer_len; ++j) {
			s[j] = ((i >> (j * 2)) & 3);
		}
		primers.push_back(s);
		primers_selection_cnt.push_back(primer_selection[i]);
		
	}
	primer_selection.clear();
	primer_selection.shrink_to_fit();
}


void MDAModel::findPrimers() {
	size_t pn = primers.size(), ppos = 0, sel = 0;
	std::deque<std::uint32_t> pcnt;
	pcnt.resize(pn, 0);
	strand_pos.clear();
	strand_pos.resize(strands.size());
	uint64_t indexed = 0;   
	uint64_t skipped = 0;
	size_t ss = strands.size(); 
	std::vector<StreamSelector<std::pair<uint32_t, int32_t>, std::mt19937_64, std::mt19937_64,false>*> selectors;
	size_t thread_cnt = threads_ps;
	std::vector<BinaryIndexer*> indexers(thread_cnt);
	selectors.reserve(primers.size());


	size_t nssmem_cnt = 0;

	size_t allocated = 0, alloc;
	std::pair<uint32_t, int32_t> **psr;
	psr = new std::pair<uint32_t, int32_t>*[primers.size()];
	for(size_t i = 0; i < primers.size(); ++i) {
		psr[i] = new std::pair<uint32_t, int32_t>[primers_selection_cnt[i]];
		allocated += sizeof(std::pair<uint32_t, int32_t>) * primers_selection_cnt[i];
		nssmem_cnt += primers_selection_cnt[i];
	}
	if(nss_mem) {
		delete[] nss_mem;
	}
	nss_mem = new uint32_t[nssmem_cnt];
	nss_cnt.clear();
	nss_cnt.resize(ss);
	std::deque<uint32_t> sz;
	sz.resize(nss_cnt.size());
#ifdef MALLOCINFO
	fprintf(stderr, "\n\nBefore ss alloc:\n");
	malloc_info(0, stderr);
#endif

	size_t total_mem_predict = allocated + thread_cnt * nssmem_cnt * 8;

	cout << "MEM_PREDICT: " << total_mem_predict << endl;
	while(total_mem_predict > mem_limit) {
		cout << "MEM_PREDICT = " << total_mem_predict << " > MEM_LIMIT = " << mem_limit << " [thread_cnt = " << thread_cnt << "]" << endl;
		thread_cnt--;
		total_mem_predict = allocated + thread_cnt * nssmem_cnt * 8;;
		if(thread_cnt < min_thread_limit) {
			cout << "NOT ENOUGH MEMORY TO CONTINUE... EXITING" << endl;
			exit(0);
		}
	}

	for(size_t i = 0; i < thread_cnt; ++i) {
		indexers[i] = new BinaryIndexer(params.primer_len);
		indexers[i]->prepare(primers);
	}

	for(size_t i = 0; i < primers.size(); ++i) {
		selectors.push_back(new StreamSelector<std::pair<uint32_t, int32_t>, std::mt19937_64, std::mt19937_64,false>(alloc, primers_selection_cnt[i], thread_cnt, rngs, urngs));
		allocated += alloc;
	}
	std::cout << "ALLOC PS:" << allocated << " (" << allocated / 1024.0/1024.0 << "MB)" << std::endl;

	omp_set_num_threads(thread_cnt);
	size_t badc = 0;
#pragma omp parallel for reduction(+:indexed,ppos,badc,skipped) schedule(dynamic, 1000)
	for (size_t i = 0; i < ss; ++i) {
		int thread = omp_get_thread_num();
		BinaryIndexer &indexer = *indexers[thread];//(params.primer_len);

		Strand& s = *strands[i];
		s_ptr_use sp = strands[i];
		ls_ptr_use curr_link = s.links;
		seg_ptr curr_seg = s.segments;
#ifndef PTR32
		int seg_len = curr_seg->length();
#else
		Segment* curr_seg_ptr;
		getSeg(curr_seg, curr_seg_ptr);
		assert(curr_seg_ptr);
		int seg_len = Segment::length(curr_seg);
#endif
		ptrdiff_t pos = 0, pos_limit = s.isCyclic() ? s.length() + params.primer_len : s.length(), spos = 0, t, sdiff;
		indexer.reset();
		int idxed = 0;
		for (; pos < pos_limit; ++pos, ++spos) {
			sdiff = 0;
			int nlink = 0;
			while (curr_link && curr_link->context(sp).begin <= pos) {
//				uc = 0;
				indexer.reset();
				t = curr_link->context(sp).end + 1;
				sdiff += t - pos;
				assert(sdiff >= 0);
				pos = t;
				curr_link = curr_link->context(sp).next;
				if(idxed < 6)
					idxed = 0;
			}
			skipped += sdiff;
			if(pos >= pos_limit) {
				indexer.reset();
				break;
			}
			if(curr_link) nlink = curr_link->context(sp).begin - params.primer_len + 1;
			else nlink = pos_limit;
			if (pos >= s.length() && s.links && s.links->context(sp).begin <= pos % s.length()) {
				indexer.reset();
				break;
			}

			if (spos == seg_len) {
				spos = 0;
#ifndef PTR32
				curr_seg = curr_seg->next;
#else
				curr_seg = curr_seg_ptr->next;
				getSeg(curr_seg, curr_seg_ptr);
				seg_len = Segment::length(curr_seg);
#endif
			}
#ifndef NDEBUG
			assert(sdiff >= 0);
#endif
#ifndef PTR32
			while (spos + sdiff >= curr_seg->length()) {
				if (!spos) {
					sdiff -= curr_seg->length();
					curr_seg = curr_seg->next;
					continue;
				}
				sdiff += spos;
				spos = 0;
			}
			seg_len = curr_seg->length();
#else
			seg_len = Segment::length(curr_seg);
			while (spos + sdiff >= seg_len) {
				if(!spos) {
					sdiff -= seg_len;
					curr_seg = curr_seg_ptr->next;
					getSeg(curr_seg, curr_seg_ptr);
					seg_len = Segment::length(curr_seg);
					continue;
				}
				sdiff += spos;
				spos = 0;
			}
			seg_len = Segment::length(curr_seg);
#endif
#ifndef NDEBUG
			assert(spos+sdiff < seg_len && spos+sdiff >= 0);
#endif
			spos += sdiff;
				while(spos < seg_len && pos < nlink && pos < pos_limit) {
					idxed++;
					assert(!curr_link || curr_link->context(sp).begin > pos);
					assert(!curr_link || curr_link->context(sp).begin > pos + params.primer_len - 1);
					assert(!(pos >= s.length() && s.links && s.links->context(sp).begin <= (pos + params.primer_len - 1) % s.length()));
#ifndef PTR32
					assert(!(spos >= curr_seg->length()));
					char cc = (*curr_seg)[spos];
#else
					assert(!(spos >= seg_len));
					char cc = (*curr_seg_ptr)[spos];
#endif
					indexed++;
#ifndef NDEBUG
					assert(utils::check(cc));
#endif
					int res = indexer.step(cc);
					if (res != -1) {
						ppos++;
						selectors[res]->step({i, pos - params.primer_len + 1}, thread);
					}
					pos++; spos++;

				}
		}
		if(idxed < 6)
			badc++;
	}
	std::cout << "Bad strands: " << (double)badc/(double)strands.size() << std::endl;
	std::cout << "Different primer types: " << primers.size() << std::endl;
	std::cout << "Indexed: " << indexed << std::endl << "Skipped: " << skipped << std::endl <<  "Index ratio: " << (double)indexed/(double)(skipped+indexed) << std::endl;
	for(size_t i = 0; i < thread_cnt; ++i)
		delete indexers[i];
#ifdef MALLOCINFO
	fprintf(stderr, "\n\nAfter indexing:\n");
	malloc_info(0, stderr);
#endif
	#pragma omp parallel for schedule(dynamic)
	for(size_t primer = 0; primer < pn; ++primer) {
		selectors[primer]->reduce();
		size_t n;
		selectors[primer]->fetchResult(n);
		selectors[primer]->clear();
		memcpy(psr[primer], selectors[primer]->fetchResult(n), n * sizeof(std::pair<std::uint32_t, std::uint32_t>));
#if 0
		selectors[primer]->freeResult();
#endif
	}
	malloc_trim(0);
#ifdef MALLOCINFO
	fprintf(stderr, "\n\nAfter copying:\n");
	malloc_info(0, stderr);
#endif
		
	for (size_t primer = 0; primer < pn; ++primer) {
		size_t n;
		auto ptr = selectors[primer]->fetchResult(n, false);
		ptr = psr[primer];
		//selectors[primer]->clear();
		pcnt[primer] = n;
		for(size_t i = 0; i < pcnt[primer] && i < primers_selection_cnt[primer]; ++i)
			nss_cnt[ptr[i].first]++;
	}
	uint32_t *ptr = nss_mem;
	for (size_t i = 0; i < ss; ++i) {
		strand_pos[i] = nss_cnt[i] ? ptr : 0;
		ptr += nss_cnt[i];
	}
	for(size_t i = 0; i < nss_cnt.size(); ++i)
		sz[i] = nss_cnt[i];
	for (size_t primer = 0; primer < pn; ++primer) {
		size_t n;
		auto ptr = selectors[primer]->fetchResult(n, false);
		ptr = psr[primer];
		for (size_t i = 0; i < pcnt[primer] && i < primers_selection_cnt[primer]; ++i) {
			strand_pos[ptr[i].first][--sz[ptr[i].first]] =  ptr[i].second;
			sel++;
		}
		delete selectors[primer];
		delete[] psr[primer];
	}
	delete[] psr;
	malloc_trim(0);
#ifdef MALLOCINFO
	fprintf(stderr, "\n\nAfter counting:\n");
	malloc_info(0, stderr);
#endif

	#pragma omp parallel for schedule(dynamic)
	for(size_t i = 0; i < ss; ++i) {
		if(nss_cnt[i])
			sort(strand_pos[i], strand_pos[i] + nss_cnt[i]);
	}
	cout << sel << endl;
	std::ostream& o_log = *o_log_p;
	o_log << "Primer positions found:" << ppos << endl;
	if(!reusePrimers) {
		printf("cleaned primers\n");
		pcnt.clear();
		pcnt.shrink_to_fit();
		primers.clear();
		primers.shrink_to_fit();
		primer_selection.clear();
		primer_selection.shrink_to_fit();
		primers_selection_cnt.clear();
		primers_selection_cnt.shrink_to_fit();
	}
}

void MDAModel::removeBadPrimers(size_t strand, size_t thread) {
		strands[strand]->last_ns_evt = 0;
		s_ptr_use sp = strands[strand];
		Strand& s = *sp;
		uint32_t pl = params.primer_len, min_pos = s.length(), max_pos = ~0u;
		Strand* first = 0;
		// now - attach all primers selected:
		size_t idx = 0;
		for(size_t ite = 0; ite < nss_cnt[strand]; ++ite) {
			uint32_t* it = strand_pos[strand] + ite;
			ptrdiff_t begin = *it;
			ptrdiff_t end = begin + pl - 1;
			// if is covered by new strands - skip this position
			if(begin < max_pos + 1u || (end >= s.len && end % s.len >= min_pos))
				continue;
			max_pos = end;
			if(!first) {
				min_pos = begin;
				first = (Strand*)(void*)~0u;
			}
			strand_pos[strand][idx++] = *it;
		}
		nss_cnt[strand] = idx;
}

double MDAModel::computeCoverage(const string& output_file) {
	double s = 0.0, sl = 0.0;
	cout << "COV" << endl;

	for(auto it = strings.begin(); it != strings.end(); ++it) {
		DNAString& ds = **it;
		std::deque<int64_t> cs[2], cs_t[2];

		uint64_t len = ds.length();
		int64_t sum[2], sum_t[2];
		cout << "COMPUTE" << endl;
		for(int i = 0; i < 2; ++i) {
			cs[i].resize(len + 1);
			cs_t[i].resize(len + 1);
			cs[i][0] = ds.cnt[i][0];
			cs_t[i][0] = ds.cnt_t[i][0];
			sum[i] = cs[i][0];
			sum_t[i] = cs_t[i][0];
			assert(sum_t[i]>=0);
			assert(sum[i]>=0);
			for (size_t j = 1; j < ds.cnt[i].size(); ++j) {
				cs[i][j] = cs[i][j - 1] + ds.cnt[i][j];
				cs_t[i][j] = cs_t[i][j - 1] + ds.cnt_t[i][j];
				assert(cs[i][j]>=0);
				assert(cs_t[i][j]>=0);
#ifndef NDEBUG
				int64_t old = sum[i], old_t = sum_t[i];
#endif
				sum[i] += cs[i][j];
				sum_t[i] += cs_t[i][j];
#ifndef NDEBUG
				//IMPORTANT: this may be false (detaching long seg)
				 assert(sum[i] >= old);
				// this should be true
				if(sum_t[i] < old_t) {
					std::cout << std::endl;
					std::cout << i << " " << sum_t[i] << " " << old_t << std::endl;
					std::cout << std::endl;
				}
				assert(sum_t[i] >= old_t);
#endif
			}
		}
		cout << "OUTPUT" << endl;
		if(output_file.size()) {
			ofstream os(output_file);
			os << "G,S,P" << endl;
			for(size_t i = 0; i < len; ++i) {
				os << (((*dna_string)[0][i] & 2) ? 1 : 0) << ", ";
				os << (cs_t[0][i] + cs_t[1][len - 1 - i]) << ", ";
				os << (cs[0][i] + cs[1][len - 1 - i]) << endl;
			}
			os << endl;
			os.close();
		}
		s += sum_t[0];
		s += sum_t[1];
		cout << "int64_t:" << (sum_t[0] + sum_t[1]) << endl;
		cout << "int64_t" << len << endl;
		scale = (sum_t[0] + sum_t[1]) / len;
		cout << "Scale: " << scale << endl;
		sl += len;
	}
	cout << s << " " << sl << endl;
	cout << "COV_END" << endl;
	return s / sl;
}

// A overtakes B
bool MDAModel::calculateOvertake(s_ptr_store A_, s_ptr_store B_, int curr_time, uint32_t& overtake_time) {
	s_ptr_use A = A_, B = B_;
#ifndef NDEBUG
	assert(A->ns && B->ns);
#endif
	overtake_time = ~0u;
	int dist_curr = distToNext(A, curr_time);
	if((!B->ns->eol && A->ns->event_time >= B->ns->event_time && dist_curr > 0) || A == B) {
		return false;
	}

	if (!A->ns->parent->isCyclic() && A->ns->start_pos > B->ns->start_pos) {
		return false;
	}

	if(dist_curr == 0) {
		overtake_time = curr_time;
		return true;
	}

	int mtime = min(B->ns->eol ? INT32_MAX : B->ns->next_time, A->ns->next_time);
	int tl = max(A->ns->last_time, B->ns->last_time), tr = mtime, tm;
	if (tl > tr)
		return false;
#ifndef NDEBUG
	assert(distToNext(A, tl) > 0);
#endif
	if (distToNext(A, tr, true) > 0)
		return false;

	while(tr - tl > 1) {
		tm = tr / 2 + tl / 2 + (tl & tr & 1);
		dist_curr = distToNext(A, tm, true); 
		if(dist_curr > 0)
			tl = tm;
		else
			tr = tm;
	}
	dist_curr = distToNext(A, tr);
#ifndef NDEBUG
	assert(dist_curr == 0 || A->ns->eol);
#endif
	overtake_time = tr;
	return true;
}

int MDAModel::stepsUpdate(s_ptr_store s, uint32_t time) {
#ifndef NDEBUG
	assert(s->ns);
	assert(time >= s->ns->last_time);
#endif
	int32_t upd = 0;
	ns_ptr_use ns = s->ns;
	if (ns->eol) {
		upd = ns->last_pos - ns->next_pos + (time < ns->next_time ? (time - ns->last_time) / ns->event_time : ns->next_pos - ns->last_pos);
	} else {
		if (ns->last_pos != ns->next_pos)
			upd = ns->last_pos - ns->next_pos + (time - ns->last_time) / ns->event_time;
		else 
			upd = 0;
	}
	uint32_t steps_old_ = ns->steps - (ns->next_pos - ns->last_pos);
	if (ns->next_pos != ns->last_pos) {
		steps_old_ += (min(ns->next_time, time) - ns->last_time) / ns->event_time;
	}
#ifndef NDEBUG
	assert(steps_old_ == ns->steps + upd);
#endif
	return upd;
}

int MDAModel::distToNext(s_ptr_store curr, uint32_t time, bool bs) {
	s_ptr_use A = curr, B = curr->ns->next_new;
#ifndef NDEBUG
	assert(time <= A->ns->next_time || A->ns->eol);
	assert(time <= B->ns->next_time || B->ns->eol);
#endif
	// ns->steps - already updated.
	// so we need to substract some steps
	int A_steps = stepsUpdate(A, time);
	int B_steps = stepsUpdate(B, time);
	int dist = A->ns->dist_next - A->ns->steps - A_steps + B->ns->steps + B_steps;
#ifndef NDEBUG
	assert(bs || dist >= 0);
#endif
	return dist;
}

void MDAModel::checkBonds(s_ptr_store parent) {
}

void MDAModel::prepareDeleteSet(std::vector<uint32_t> &begins, std::vector<uint32_t>& ends, s_ptr_store parent) {
	s_ptr_use base = parent->last_ns_evt, curr, nxt;
#ifndef NDEBUG
	assert(base && base->ns);
#endif
	curr = base;
	nxt = curr->ns->prev_new;
#ifndef NDEBUG
	assert(nxt->ns);
#endif
	int last_end = curr->ns->next_pos, last_begin = curr->ns->next_pos;
	std::vector<uint32_t> b[2], e[2];
	int idx = 0;
	uint32_t curr_pos, prev_pos;
#ifndef NDEBUG
	if (debug >= 10) {
		do {
			curr_pos = curr->ns->next_pos;
			assert((curr->ns->start_pos + curr->ns->steps) % parent->len == curr->ns->next_pos);
			assert((curr->ns->next_pos + curr->ns->next_new->ns->steps - curr->ns->steps + curr->ns->dist_next + parent->len * 100) % parent->len == curr->ns->next_new->ns->next_pos);
			assert(curr->ns->next_pos != curr->ns->next_new->ns->next_pos || curr == curr->ns->next_new);
			curr = curr->ns->prev_new;
		} while (curr != base);
	}
#endif
	bool seg_ends = false;
	if (strands[0] == parent && steps != 0)
		cout << "NEW STRANDS ON MAIN(!)" << endl;
#ifndef NDEBUG
	assert(curr);
#endif
	// compute delete list
	do {
		curr_pos = curr->ns->next_pos;
		prev_pos = nxt->ns->next_pos;
#ifndef NDEBUG
		assert(curr_pos < parent->len && prev_pos < parent->len);
#endif
		int dist = (curr_pos + parent->len - prev_pos) % parent->len;
#ifndef NDEBUG
		assert(dist >= 1 || nxt == base);
		assert(nxt == base || dist == distToNext(nxt, INT_MAX));
		assert(dist + nxt->ns->steps > curr->ns->steps || nxt == base);
#endif
		if (nxt == base)
			dist = parent->len;

		seg_ends = false;
		assert(dist >= 0);
		if ((uint32_t)dist <= curr->ns->steps) {
			// segment continues
		} else {
			// segment stops
			last_begin = curr->ns->next_pos - curr->ns->steps;
			last_begin = last_begin >= 0 ? last_begin : (last_begin - (last_begin / parent->len - 1) * parent->len);
#ifndef NDEBUG
			assert(last_begin >= 0);
#endif
			last_begin %= parent->len;
			seg_ends = true;
			//o_log << "ENDS!" << endl;
			if (last_begin < last_end) {
				b[idx].push_back(last_begin);
				e[idx].push_back(last_end);
			}
			else {
				assert(idx == 0);
				b[idx].push_back(0);
				e[idx].push_back(last_end);
				idx++;
				b[idx].push_back(last_begin);
				e[idx].push_back(parent->len - 1);
			}
			last_end = nxt->ns->next_pos;
		}
		curr = curr->ns->prev_new;
		nxt = curr->ns->prev_new;
	} while (curr != base);
	if (!seg_ends) {
		//[base->next_pos + 1 seg_end]
		last_begin = (base->ns->next_pos + 1) % parent->len;
		if (last_begin < last_end) {
			b[idx].push_back(last_begin);
			e[idx].push_back(last_end);
		}
		else {
			assert(idx == 0);
			b[idx].push_back(0);
			e[idx].push_back(last_end);
			idx++;
			b[idx].push_back(last_begin);
			e[idx].push_back(parent->len - 1);
		}
	}
#ifndef NDEBUG
#if 0
	if (debug >= 1) 
#endif
	{
		std::set<int> bset;
		bset.insert(b[0].begin(), b[0].end());
		bset.insert(b[1].begin(), b[1].end());
		assert(bset.size() == b[0].size() + b[1].size());
		std::set<int> eset;
		eset.insert(e[0].begin(), e[0].end());
		eset.insert(e[1].begin(), e[1].end());
		assert(eset.size() == e[0].size() + e[1].size());
	}
#endif
	begins = std::move(b[0]);
	begins.insert(begins.end(), b[1].begin(), b[1].end());
	ends = std::move(e[0]);
	ends.insert(ends.end(), e[1].begin(), e[1].end());
	sort(begins.begin(), begins.end());
	sort(ends.begin(), ends.end());
	assert(ends.size() == begins.size());

}


void MDAModel::deleteSet(vector<uint32_t> &begins, vector<uint32_t> &ends, s_ptr_store parent) {
	int del_idx = 0, segments = begins.size();
	ls_ptr_use ls = parent->links;
	seg_ptr s = parent->segments;
#ifdef PTR32
	Segment* sptr;
	getSeg(s, sptr);
	assert(sptr);
#endif
	uint32_t strand_pos = 0;
	while (ls) {
		uint32_t ls_begin = ls->context(parent).begin;
		BondSegment::BondContext ctx = ls->context(parent);
		while (del_idx < segments && ls_begin > ends[del_idx]) del_idx++;
		if (del_idx == segments) break;
		ls_ptr_use nxt = ctx.next;
#ifndef NDEBUG
		assert(ends[del_idx] >= begins[del_idx]);
		assert(!nxt || nxt->context(parent).begin > ctx.end);
#endif
		if (ctx.end >= begins[del_idx]) {
			uint32_t ls_end = ctx.end;
			assert(begins[del_idx] <= ls_begin);
#ifndef PTR32
			while (ls_begin >= strand_pos + s->length()) {
				strand_pos += s->length();
				s = s->next;
			}
#else
			while (ls_begin >= strand_pos + Segment::length(s)) {
				strand_pos += Segment::length(s);
				s = sptr->next;
				getSeg(s, sptr);
			}
#endif
			uint32_t seg_pos = ls_begin - strand_pos;
#ifndef PTR32
			uint32_t delete_cnt = min((int)s->length() - seg_pos, min(ends[del_idx], ls_end) - ls_begin + 1);
#else
			uint32_t delete_cnt = min((int)Segment::length(s) - seg_pos, min(ends[del_idx], ls_end) - ls_begin + 1);
#endif
#ifndef NDEBUG
			assert(ls_begin >= strand_pos);
			assert(delete_cnt > 0);
#endif
			if (ctx.other == strands[0] && parent == strands[0])
				steps = -1;
			StringSegment *ss = nullptr;
#ifndef PTR32
			if(s->ss)
				ss = static_cast<StringSegment*>(s);
#else
			Segment* ssp;
			if(getSeg(s, ssp))
				ss = static_cast<StringSegment*>(ssp);

#endif
			if (ss)
				ss->changeBond(seg_pos, seg_pos + delete_cnt);
			if (delete_cnt == ls->length()) {
				ls->del(ls);
// Reusable 64-bit pointers are not available
#ifdef PTR32
				ls32_allocator.deleted(0, ls);
#endif
			} else {
				nxt = ls;
				BondSegment::BondContext& lc = ls->context(parent);
				BondSegment::BondContext& lc_o = ls->context(lc.other);
#ifndef NDEBUG
				if (lc.next)
					assert(lc.next->context(parent).begin > lc.end);
				if (lc.prev)
					assert(lc.prev->context(parent).end < lc.begin);
#endif
				lc.begin += delete_cnt;
#ifndef NDEBUG
				assert(lc.begin <= lc.end);
#endif
				lc_o.end -= delete_cnt;
#ifndef NDEBUG
				assert(lc_o.begin <= lc_o.end);
				if (lc.next)
					assert(lc.next->context(parent).begin > lc.end);
				if (lc.prev)
					assert(lc.prev->context(parent).end < lc.begin);
#endif
			}
		}
		ls = nxt;
	}
}

void MDAModel::prepareNewBonds(s_ptr_store parent, ls_ptr_use &new_links) {
	s_ptr_use base = parent->last_ns_evt, curr;
	s_ptr_use nxt = base->ns->prev_new;
	uint32_t idx = 0, curr_pos, prev_pos;
	/* delete from old link set, take coverage into account */
	// compute new list
	curr = base;
	ls_ptr_use ls_right = 0, ls_left = 0, ls_start = 0;
	uint32_t lp = INT32_MAX;
	do {
		uint32_t steps_t = 0;
		uint32_t pos_self = 0;
		uint32_t pos_parent = curr->ns->next_pos;
		curr_pos = curr->ns->next_pos;
		prev_pos = nxt->ns->next_pos;
		prev_pos = (prev_pos + 1) % parent->len;
		uint32_t expected_pos = curr_pos;
		uint32_t dist = (curr_pos + parent->len - prev_pos) % parent->len;
		ls_ptr_use last_link = 0;
		ls_ptr_use old = 0;
		uint32_t i = curr->ns->strand_events.size() - 1;
		for(auto it = curr->ns->strand_events.rbegin(); it != curr->ns->strand_events.rend() && steps_t < dist && i != ~0u; ++it, --i) {
			StrandEvent evt = *it;

			if (evt.evt_type == SEvent::ADD && i != curr->ns->strand_events.size() - 1) {
				expected_pos++;
				pos_parent++;
				expected_pos %= parent->len;
				pos_parent %= parent->len;
			}
#ifndef NDEBUG
			assert(evt.strand_pos == expected_pos);

			//			assert((parent->len + pos_parent) % parent->len == evt.strand_pos); TODO: find correct (2xADD+EOL case)
#endif
			uint32_t evtlen = evt.end - evt.begin + 1;
			expected_pos += parent->len - evtlen;
			pos_parent -= evtlen;
			if (evt.evt_type == SEvent::ADD){
				expected_pos--;
				pos_parent--;
			}
			int pos_self_ = pos_self;
			pos_self += (evt.evt_type == SEvent::SKIP || evt.evt_type == SEvent::EOL) ? 0 : evtlen;
			steps_t += (evt.evt_type != SEvent::EOL && evt.evt_type != SEvent::ADD) ? evtlen : 0;
			expected_pos %= parent->len;

			if (evt.evt_type != SEvent::OK) {
				old = 0;
				continue;
			}

			uint32_t max_len = (evt.strand_pos - prev_pos + parent->len) % parent->len + 1;
			uint32_t link_len = min(max_len, evtlen);

			StringSegment* ss;
			uint32_t link_end = evt.strand_pos;
			uint32_t link_begin = evt.strand_pos - link_len + 1;
#ifndef NDEBUG
			assert(link_begin >= evt.strand_pos + evt.begin - evt.end);
			uint32_t strand_begin = evt.strand_pos + evt.begin - evt.end;
			assert(link_begin == prev_pos || link_begin == strand_begin);
#endif
			uint32_t seg_begin = evt.end - link_len + 1;

#ifndef PTR32
			if(evt.src->ss)
				ss = static_cast<StringSegment*>(evt.src);
			else
				ss = nullptr;
#else
			Segment* ssp;
			bool sss = getSeg(evt.src, ssp);
			ss = sss ? static_cast<StringSegment*>(ssp) : nullptr;
#endif

			if (ss) {
				ss->beginBond(seg_begin);
				ss->endBond(evt.end);
			}
			ls_ptr_use appendA = 0;
			if (evt.strand_pos + 1 >= link_len){
				// non-separating link
				if (old && old->context(parent).begin >= link_len) {
#ifndef NDEBUG
					assert(old->context(parent).begin > evt.strand_pos);
					assert((evt.strand_pos + 1) % parent->len == old->context(parent).begin);
#endif
					old->context(parent).begin -= link_len;
					old->context(old->context(parent).other).end += link_len;
				} else {
					std::uint32_t id;
#ifndef PTR32
					BondSegment* appendA_ptr = appendA = ls_allocator.allocateNew(0);
#else
					BondSegment* appendA_ptr = ls32_allocator.allocateNew(0, id);
					appendA = id;

#endif
					new (appendA_ptr) BondSegment(parent, curr, link_begin, link_end, pos_self_, pos_self_ + link_len - 1);
					if (last_link) {
						last_link->context(curr).next = appendA;
						appendA->context(curr).prev = last_link;
						assert(last_link->context(curr).end < appendA->context(curr).begin);
						last_link = appendA;
					}
					else curr->links = last_link = appendA;
					old = appendA;
				}
				if (appendA) {
					if (lp < evt.strand_pos) idx++;
#ifndef NDEBUG
					assert(idx <= 1);
#endif
					if (idx) {
						//assert(ls_right);
						if (ls_right)
							ls_right->context(parent).prev = appendA;
						appendA->context(parent).next = ls_right;
						ls_right = appendA;
					}
					else {
						if (!ls_start)
							ls_start = appendA;
						if (ls_left)
							ls_left->context(parent).prev = appendA;
						appendA->context(parent).next = ls_left;
						ls_left = appendA;
					}
				}
				lp = evt.strand_pos;
			}
		}
		curr = curr->ns->prev_new;
		nxt = curr->ns->prev_new;
	} while (curr != base);
	if (ls_start)
		ls_start->context(parent).next = ls_right;
	if (ls_right)
		ls_right->context(parent).prev = ls_start;
	new_links = ls_left ? ls_left : ls_start ? ls_start : ls_right;
}

void MDAModel::mergeBonds(s_ptr_store parent, ls_ptr_use new_links) {
	// MERGE
	ls_ptr_use bb, cc;
	bb = new_links;
	cc = parent->links;
	ls_ptr_use start = 0, current = 0;
	while (cc || bb) {
		while (cc && (!bb || cc->context(parent).begin < bb->context(parent).begin)) {
#ifndef NDEBUG
			assert(!bb || cc->context(parent).end < bb->context(parent).begin);
#endif
			cc->context(parent).prev = current;
			if (current) {
				current->context(parent).next = cc;
#ifndef NDEBUG
				assert(current->context(parent).end < cc->context(parent).begin);
#endif
			}
			else
				start = current = cc;
			current = cc;
			cc = cc->context(parent).next;
		}
		if (bb) {
			bb->context(parent).prev = current;
			if (current) {
				current->context(parent).next = bb;
#ifndef NDEBUG
				assert(current->context(parent).end < bb->context(parent).begin);
#endif
			}
			else
				start = current = bb;
			current = bb;
			bb = bb->context(parent).next;
		}
	}
	parent->links = start;
	cc = parent->links;
#ifndef NDEBUG
	while (cc) {
		assert(!cc->context(parent).next || cc->context(parent).next->context(parent).begin > cc->context(parent).end);
		cc = cc->context(parent).next;
	} 
#endif
}
