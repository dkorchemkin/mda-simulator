#include "buildconfig.h"
#include "Segment.h"
#include "BondSegment.h"

bool getSeg(seg_ptr ptr, Segment* &res) {
#ifndef PTR32
	res = ptr;
	return !res || res->ss;
#else
	StringSegment* ss = ss32_allocator.convert(ptr);
	ErrorSegment* es = es32_allocator.convert(ptr);
	res = ss ? (Segment*)ss : (Segment*)es;
	return !res || ss;
#endif
}

#ifdef PTR32
BlockAllocator<threads, StringSegment, true, true, false,  2> ss32_allocator(0);
BlockAllocator<threads, ErrorSegment, true, true, false, 2>  es32_allocator(1);
BlockAllocator<1, BondSegment, true, true, true, 1> ls32_allocator;
BlockAllocator<threads, Strand, true, true, false, 1>  s32_allocator;
BlockAllocator<threads, NS_info, false, true, false, 1>  ns32_allocator;
#endif
