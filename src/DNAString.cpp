#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <cassert>
#include <cstdint>

#include "DNAString.h"
#include "utils.h"
#include "mda_params.h"

using namespace std;
std::ostream& operator<<(std::ostream& os, const DNAString& ds) {
	os << "DNA String: [ "  << &ds << " (" << ds.length() << ")]" << std::endl;
	os << ds.forward << std::endl;
	os << ds.reverse << std::endl;
	return os;
}

const uint32_t DNAString::MAX_SEQ = 1000;
const uint32_t DNAString::MAX_SEG = 100000000;

size_t DNAString::length() const {
	return forward.size();
}

size_t DNAString::reverseIdx(const size_t& idx) const {
	size_t len = length();
	return len - 1 - (idx % len);
}

const std::string& DNAString::operator[](size_t complementary) const {
	return complementary ? reverse : forward;
}

DNAString::DNAString(const std::string& forward_str, mda_params params) : forward(forward_str) {
	utils::string_transform(forward, reverse);
	cnt[0].resize(forward.size()+1);
	cnt[1].resize(reverse.size()+1);
	cnt_t[0].resize(forward.size() + 1);
	cnt_t[1].resize(reverse.size() + 1);
	int len = length();
	int cscp_cnt = (len + MAX_SEQ - 1) / MAX_SEQ;
	cum_prod[0].resize(cscp_cnt);
	cum_prod[0].shrink_to_fit();
	cum_prod[1].resize(cscp_cnt);
	cum_prod[1].shrink_to_fit();
	cp_cum_sum[0].resize(cscp_cnt);
	cp_cum_sum[0].shrink_to_fit();
	cp_cum_sum[1].resize(cscp_cnt);
	cp_cum_sum[1].shrink_to_fit();

	for (int i = 0; i < cscp_cnt; ++i) {
		uint32_t lb = MAX_SEQ * i;
		uint32_t sz = min(MAX_SEQ, len - lb);
		uint32_t idx = (forward[lb]);
		cum_prod[0][i].resize(sz);
		cum_prod[0][i].shrink_to_fit();
		cp_cum_sum[0][i].resize(sz);
		cp_cum_sum[0][i].shrink_to_fit();

		cum_prod[0][i][0] = params.p_ok_copy(idx);
		for (uint32_t j = 1; j < sz; ++j) {
			idx = (forward[lb + j]);
			cum_prod[0][i][j] = cum_prod[0][i][j - 1] * params.p_ok_copy(idx);
		}

		idx = (forward[lb]);
		cp_cum_sum[0][i][0] = params.p_error_copy(idx);
		for (uint32_t j = 1; j < sz; ++j) {
			idx = (forward[lb + j]);
			cp_cum_sum[0][i][j] = cp_cum_sum[0][i][j - 1] + cum_prod[0][i][j - 1] * params.p_error_copy(idx);
		}
	}
	int lb = 0;
	for (int i_ = cscp_cnt - 1, i = 0; i_ >= 0; i_--, i++) {
		uint32_t lb_ = MAX_SEQ * i_;
		uint32_t sz = min(MAX_SEQ, len - lb_);
		uint32_t ridx = (reverse[lb]);

		cum_prod[1][i].resize(sz);
		cum_prod[1][i].shrink_to_fit();
		cp_cum_sum[1][i].resize(sz);
		cp_cum_sum[1][i].shrink_to_fit();
		cum_prod[1][i][0] = params.p_ok_copy(ridx);

		for (uint32_t j = 1; j < sz; ++j) {
			ridx = (reverse[lb + j]);
			cum_prod[1][i][j] = cum_prod[1][i][j - 1] * params.p_ok_copy(ridx);
		}

		ridx = (reverse[lb]);
		cp_cum_sum[1][i][0] = params.p_error_copy(ridx);
		for (uint32_t j = 1; j < sz; ++j) {
			ridx = (reverse[lb + j]);
			cp_cum_sum[1][i][j] = cp_cum_sum[1][i][j - 1] + cum_prod[1][i][j - 1] * params.p_error_copy(ridx);
		}

		lb += sz;
	}
}

// first error if prob = p
size_t DNAString::succ_copy(size_t l, size_t r, size_t dir, double p, bool &next_ok) {
	uint32_t bin, lb, rb;
	if (dir) {
		uint32_t rem;
		if ((rem = this->length() % MAX_SEQ)) {
			uint32_t rem = this->length() % MAX_SEQ;
			bin = l < rem ? 0 : 1 + (l - rem) / MAX_SEQ;
			lb = l < rem ? 0 : (bin - 1) * MAX_SEQ + rem;
			rb = l < rem ? rem - 1 : (lb + MAX_SEQ - 1);
		}
		else {
			bin = l / MAX_SEQ;
			lb = bin * MAX_SEQ;
			rb = lb + MAX_SEQ - 1;
		}
	} else {
		bin = l / MAX_SEQ;
		lb = bin * MAX_SEQ;
		rb = lb + MAX_SEQ - 1;
		if(rb + 1 > this->length())
			rb = this->length() - 1;
	}
#ifndef NDEBUG
	assert(cp_cum_sum[dir].size() > bin);
	assert(cp_cum_sum[dir][bin].size() == rb - lb + 1);
#endif

	if(rb > r)
		rb = r;
#ifndef NDEBUG
	assert(lb <= l);
	assert(rb >= l);
	assert(rb - lb <= MAX_SEQ);
#endif
	double denom = l > lb ? cum_prod[dir][bin][l - 1 - lb] : 1.0;
	double sub = l > lb ? cp_cum_sum[dir][bin][l - 1 - lb] : 0.0;
	next_ok = false;
	if((cp_cum_sum[dir][bin][l - lb] - sub) / denom > p)
		return 0;
	size_t ll = l, rr = rb + 1, m;
	while(rr - ll > 1) {
		m = (ll + rr) / 2;
#ifndef NDEBUG
		assert(cp_cum_sum[dir][bin].size() > m - lb);
#endif
		if((cp_cum_sum[dir][bin][m - lb] - sub) / denom > p) // (cs - cs) / cp < p
			rr = m;
		else
			ll = m;
	}
	if(rr == rb + 1)
		next_ok = true;
	return rr - l;
}
