CXX = g++
CFLAGS = -c  -Wall --pedantic -Iinclude/ -std=c++14 -march=native -O3 -g -fopenmp -fdiagnostics-color -DPTR32 -DONE_DNA_STRING -DNDEBUG -DBULKALLOC
LDFLAGS = -fopenmp
SOURCES = $(wildcard src/*.cpp)
BUILD_DIR = build/
OBJECTS = $(addprefix $(BUILD_DIR), $(SOURCES:src/%.cpp=%.o))
EXECUTABLE = mda_simulator

all: $(BUILD_DIR) $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE) : $(OBJECTS)
	$(CXX) $(LDFLAGS) $(OBJECTS) -o $@

$(OBJECTS): $(SOURCES)
	$(CXX) $(CFLAGS) src/$(*F).cpp -o $@

$(BUILD_DIR):
	mkdir $(BUILD_DIR)

docs:
	mkdir docs/
	cd doxycfg && doxygen dox.cfg

.PHONY : clean

clean:
	rm -rf docs/
	rm -rf build/
	rm -f mda_simulator
