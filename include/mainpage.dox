/**
\mainpage Multiple Displacement Amplification Simulator

# Multiple Displacement Amplification simulator

This project is a MDA simulator able to simulate up to \f$10^5\sim 10^6\f$ 
amplification of source sequences of length \f$\sim 5\cdot 10^6\f$ nucleotides.

Since \f$10^5\sim 10^6\f$ amplification results in \f$10^{11+}\f$ nucleotides,
it is necessary to use some tricky datastructures to store information about
nucleotides and bonds fitting reasonable memory limits. Time also matters a lot,
so parallelization is a desired feature

# Work In Progress Declaration
Some work is not completed yet:
- External tests
- Better concurency scaling on last step (sometimes it seems possible to 
finish step with speed tradeoff on indexing step)
- Better code documenting: some parts of code definitely lack clarification

# Data Organization
Basic idea of storing information about strands and bounds is to store 
information about continuous segments of bounds and source DNA's copies.

Bonds (BondSegment) are stored as a labelled directed graph, which is
"locally equivalent" to a double-linked list for each Strand (=if we consider
only vertices with labels corresponding this strand).

Nucleotides composing each Strand are stored as a single-linked list of
Segments; either as parts of source DNA (StringSegment) or as error-introduced
segments (ErrorSegment).

For further information on data structures consider inspecting paper: TODO

## Memory allocation
Memory for the most space-consuming data (Strands, Segments, BoundSegments) 
is allocated using BlockAllocator in order to decrease allocation overhead and 
number of standard allocator calls.

In order to gradually decrease memory usage all pointers in Strand, Segment,
BoundSegment::BoundContext are replaced with 32-bit identificators; in most 
cases it is masked with PtrWrapper and PtrStoreWrapper. 

In some cases (StringSegment and ErrorSegment) "RTTI"-like behaviour is 
implemented using BlockAllocator.

# Simulation algorithm
All simulation is held in MDAModel class. It can be decomposed into the 
following steps:

- Initial positions selection
	- Primers selection
	- Strands indexing
- 1-step MDA simulation
	- Events simulation
	- State reconstruction
		- Segments reconstruction
		- Bounds reconstruction
			- New bounds computation
			- Delete set computation
			- Removing intersection
			- Merging bounds

## Parallelization
The most time-consuming steps are "Strands indexing" and "Events simulation".

Events simulation can be run in parallel for different parent strands without
significant memory overhead independent of number of threads. The only part of 
simulation that requires synchronization is "State reconstruction", which takes 
non-significant amount of time to complete.

"Strands indexing" can be run in parallel for different parent strands, but
significant memory overhead (proportional to number of threads) is introduced. 
This is exceptionally important on the last step (sometimes it seems that it is 
possible to finish step, but current implementation of concurency downscaling is bad).

# Build requirements
Project is expected to be buildable on any contemporary Linux system. The 
only requirement is OpenMP implementation supported by compiler.

One might be interested in editing buildconfig.h/buildconfig.cpp for build 
customization.

# Run requirements
Depending on genome size, simulator should be able to simulate \f$\sim 3-4\f$ 
steps (\f$\sim 10^4\f$ amplification) of genome with length of \f$2-5\cdot 10^6\f$ 
on any contemporary system (4 cores / 16Gb RAM) in less than 15 minutes.

For large simulations (\f$10^{5+}\f$ amplification) more RAM is required (and more 
cores are desired); on Amazon's r3.8xlarge (16C, 240GB) we were able to simulate 
\f$>5\cdot 10^5\f$ amplification of sequences: NC, KD, MS.

# Simulator output
Simulator produces multiple human-readable logs and csv's containing AT/GC 
indicator, total amplification and number of bonded pairs corresponding to this position.
*/
