#ifndef BLOCKALLOCATOR_H
#define BLOCKALLOCATOR_H

#include <vector>
#include <cstdint>
#include <deque>
#include <cassert>
#include <iostream>
#include <typeinfo>


static_assert(sizeof(size_t) == 8, "Will not compile on 32-bit platform");

constexpr size_t orRight(size_t a) {
	return a ? a | orRight(a >> 1) : 0;
}

constexpr size_t nextPow2Mask(size_t a) {
	return (a == 0 ? 0 : orRight(a - 1));
}

constexpr size_t prevPow2(size_t a) {
	return a <= 1 ? 0 : prevPow2(a >> 1) + 1;
}

constexpr size_t nextPow2Size(size_t a) {
	return a ? prevPow2(nextPow2Mask(a) + 1) : 0;
}

//
// Address structure: [data_type][thread_id][block_id][block_offset]

/*!
 * \brief Allocates items in huge blocks and provides pointers for placement-new construction on request
 *
 * This class is an allocator for all memory-consuming structures such as \ref{Segment} (\ref{StringSegment},\ref{ErrorSegment}), \ref{Strand}, \ref{BondSegment}, \ref{NSInfo}.
 *
 * Items are allocated in \f$2^{20}\f$ blocks, each thread has dedicated pool.
 * 
 * If PTR_32 mode is enabled, items may be referenced by 32-bit indexes of form \f$\langle Ty,Th,B,O \rangle\f$ where \f$Ty\f$ - type (for single-type indexes this field has zero length), \f$Th\f$ - thread index, \f$B\f$ - block index, \f$O\f$ - offset inside block.
 * If allow_reuse is false, no deallocation happens; in allow_reuse mode "deallocated" items may be reused, if index is provided
 *
 * If reserve_null is true, 0 index is reserved
 */
template<size_t threads, typename T, bool simple = true, bool reserve_null = false, bool allow_reuse = false, size_t typenames = 1, typename TI = std::uint32_t>
class BlockAllocator {
private:
	size_t type;
	std::deque<std::deque<T*>> blocks;
	std::deque<size_t> used, curr;
	std::deque<std::vector<TI>> empty;
	static const size_t BLOCK_SIZE = 1048576;
	static const size_t INBLOCK_MASK = nextPow2Mask(BLOCK_SIZE);
	static const size_t INBLOCK_SHIFT = nextPow2Size(BLOCK_SIZE);
	static const size_t THREAD_SHIFT = nextPow2Size(threads);
	static const size_t THREAD_MASK = (1LLU << (THREAD_SHIFT)) - 1LLU;
	static const size_t TYPENAME_SHIFT = nextPow2Size(typenames);
	static const size_t TYPENAME_MASK = (1LLU << TYPENAME_SHIFT) - 1LLU;
	static const size_t BLOCK_SHIFT = sizeof(TI) * 8 - INBLOCK_SHIFT - THREAD_SHIFT - TYPENAME_SHIFT;
	static_assert(BLOCK_SHIFT < 64, "Consider increasing index type size");
	static const size_t BLOCK_MASK = (1LLU << (BLOCK_SHIFT)) - 1LLU;
	static_assert(TYPENAME_SHIFT + BLOCK_SHIFT + THREAD_SHIFT + INBLOCK_SHIFT == sizeof(TI) * 8, "WTF?!");
public:
	/*!
	 * \brief Constructs BlockAllocator for requested type idx
	 * \param type Type index (if multiple types are used)
	 */
	BlockAllocator(size_t type = 0) : type(type),  blocks(threads), used(threads), curr(threads), empty(threads) {
		if(reserve_null) {
			for(size_t i = 0; i < threads; ++i) {
				curr[i] = 0;
				used[i] = 1;
			}
		}
	}
	//! Destructor actually frees memory
	~BlockAllocator() {
		free();
	}
	/*!
	 * \brief Provides pointer for placement-new construction
	 *
	 * \param thread Thread index
	 * \param addr Reference to store index of "allocated" item
	 * \return Pointer for object
	 *
	 * If allow_reused and reusable items available - returns one of them, otherwise if there is unused item in current block - returns it, otherwise - allocates new block
	 */
	T* allocateNew(size_t thread, TI &addr) {
		assert(thread < threads);
		if(allow_reuse && empty[threads].size()) {
			addr = *empty[threads].rbegin();
			empty[threads].resize(empty[threads].size() - 1);
			return convert(addr);
		}
		if(used[thread] == BLOCK_SIZE || blocks[thread].size() == 0) {
			bool first_block = blocks[thread].size() == 0;
			if(blocks[thread].size() == 0 || ++curr[thread] == blocks[thread].size())
				blocks[thread].push_back((T*)operator new[] (sizeof(T) * BLOCK_SIZE));
			used[thread] = first_block && reserve_null ? 1 : 0;
		}
		addr = type;
		addr <<= THREAD_SHIFT;
		addr |= thread;
		addr <<= BLOCK_SHIFT;
		addr |= curr[thread];
		addr <<= INBLOCK_SHIFT;
		addr |= used[thread];

		assert(type <= TYPENAME_MASK);
		assert(thread <= THREAD_MASK);
		assert(curr[thread] <= BLOCK_MASK);
		assert(used[thread] <= INBLOCK_MASK);
		return blocks[thread][curr[thread]] + used[thread]++;
	}
	/*!
	 * \brief Provides pointer for placement-new construction
	 *
	 * \param addr Reference to store index of "allocated" item
	 * \return Pointer for object
	 *
	 * If there is unused item in current block - returns it, otherwise - allocates new block
	 * Note: this one can not reuse items
	 */
	T* allocateNew(size_t thread) {
		assert(thread < threads);
		if(used[thread] == BLOCK_SIZE || blocks[thread].size() == 0) {
			bool first_block = blocks[thread].size() == 0;
			if(blocks[thread].size() == 0 || ++curr[thread] == blocks[thread].size())
				blocks[thread].push_back((T*)operator new[] (sizeof(T) * BLOCK_SIZE));
			used[thread] = first_block && reserve_null ? 1 : 0;
		}
		assert(thread <= THREAD_MASK);
		assert(curr[thread] <= BLOCK_MASK);
		assert(used[thread] <= INBLOCK_MASK);
		return blocks[thread][curr[thread]] + used[thread]++;
	}
	/*!
	 * \brief Converts index to pointer
	 * \param _index Item index
	 * \return Pointer to item
	 */
	T* convert(TI _index) {
		size_t index = _index;
		size_t inblock = index & INBLOCK_MASK;
		index >>= INBLOCK_SHIFT;
		size_t block = index & BLOCK_MASK;
		index >>= BLOCK_SHIFT;
		size_t thread = index & THREAD_MASK;
		index >>= THREAD_SHIFT;
		size_t typep = index & TYPENAME_MASK;
		assert(typep <= TYPENAME_MASK);
		if(typep != type)
			return 0;
		if(reserve_null && block == 0 && inblock == 0)
			return 0;
		assert(typep < typenames);
		return blocks[thread][block] + inblock;
	}
	/*!
	 * \brief Shrinks reuse storage
	 */
	void shrink() {
		for(auto v: empty)
			v.shrink_to_fit();
	}
	/*!
	 * \brief Deallocates memory
	 *
	 * Note: you should not call any other method after this
	 */
	void free() {
		printStats();

		if(!simple) {
			size_t idx = 0;
			for(auto v: blocks) {
				for(size_t i = 0; i < v.size() && i <= curr[idx]; ++i) {
					size_t lim = i == curr[idx] ? used[idx] : BLOCK_SIZE;
					for(size_t j = i==0&&reserve_null?1:0; j < lim; ++j)
						v[i][j].~T();
				}
				idx++;
			}
		}

		for(auto v: blocks)
			for(auto p: v)
				operator delete[] (p);
		blocks.clear();
		blocks.resize(threads);
		used.clear();
		used.resize(threads, reserve_null ? 1 : 0);
		curr.clear();
		curr.resize(threads, 0);
		empty.clear();
		empty.resize(threads);
	}

	/*!
	 * \brief Clears allocated data, does not deallocate memory
	 * \param thread Thread index
	 */
	void clear(size_t thread) {
		if(!simple) {
			for(size_t i = 0; i < blocks[thread].size() && i <= curr[thread]; ++i) {
				size_t lim = i == curr[thread] ? used[thread] : BLOCK_SIZE;
				for(size_t j = reserve_null && i == 0 ? 1 : 0; j < lim; ++j)
					blocks[thread][i][j].~T();
			}
		}
		curr[thread] = 0;
		empty[thread].clear();
		used[thread] = reserve_null ? 1 : 0;
	}
	/*!
	 * \brief Marks item as reusable (only works with allow_reuse
	 * \param thread Thread index
	 * \param addr Item index
	 */
	void deleted(size_t thread, TI addr) {
		empty[thread].push_back(addr);
	}

	//! \brief Prints allocator statistics to stdout
	void printStats() {
		size_t alloc = 0;
		size_t use = 0;
		size_t idx = 0;
		for(auto v: blocks) {
			size_t allocated = v.size() * BLOCK_SIZE;
			size_t usedm = curr[idx] * BLOCK_SIZE;
			usedm += used[idx];
			idx++;
			std::cout << "(" << allocated << " : " << usedm << " : " << usedm / std::max((double)allocated, 1.0) << ", " << v.size() << ")";
			alloc += allocated;
			use += usedm;
		}
		use *= sizeof(T);
		alloc *= sizeof(T);
		alloc += (sizeof(decltype(blocks[0])) + sizeof(size_t)) * threads;
		alloc += sizeof(*this);
		std::cout << typeid(this).name() << " [ " << alloc / 1024.0 / 1024.0 << "MB : " << use / 1024.0 / 1024.0 << "MB : " << (double)use/alloc << "]" << std::endl;

	}
};

#endif
