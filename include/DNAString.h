#ifndef DNASTRING_H
#define DNASTRING_H

#include <string>
#include <ostream>
#include <vector>
#include <deque>
#include <cstdint>
#include <cstdint>

#include "mda_params.h"

using namespace std;

class Segment;

/*!
 * \brief Class for handling source DNA
 *
 * This class holds source DNA, precalculated probabilities and coverage info
 */
class DNAString {
	friend class StringSegment;
	friend class MDAModel;
public:
	/*!
	 * \brief DNAString constructor
	 * \param forward DNA in normal direction
	 * \param params mda_params for precaclulating probabilities
	 */
	DNAString(const std::string& forward, mda_params params = mda_params());
	//! \brief Returns DNAString length
	size_t length() const;
	/*! 
	 * \brief Returns complementary index
	 * \param idx Index on current strand
	 * \return Index on complementary strand
	 */
	size_t reverseIdx(const size_t& idx) const;
	/*!
	 * \brief Subscript operator
	 * \param complementary 1 if complementary strand is requested
	 * \return DNA strand
	 */
	const std::string& operator[](size_t complementary) const;
	/*!
	 * \brief Prints DNAString to output stream
	 * \param os Output stream
	 * \param ds DNAString
	 */
	friend std::ostream& operator<<(std::ostream& os, const DNAString& ds);
	//! Max OK length in 1 request
	static const uint32_t  MAX_SEQ;
	//! Max Segment length
	static const uint32_t  MAX_SEG;
	/*!
	 * \brief Models distribution of successive successive OK steps
	 * \param l Left bound
	 * \param r Right bound
	 * \param dir Complimentarity
	 * \param p U(0,1)
	 * \param next_ok Set to true if next event should be OK
	 * \return Number of successive OK steps to perform
	 */
	size_t succ_copy(size_t l, size_t r, size_t dir, double p, bool &next_ok);
private:
	std::deque<std::vector<double>> cp_cum_sum[2];
	std::deque<std::int64_t> cnt[2];
	std::deque<std::int64_t> cnt_t[2];
	std::deque<std::vector<double>> cum_prod[2];
	std::string forward, reverse;
};

#endif
