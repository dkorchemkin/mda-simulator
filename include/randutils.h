#ifndef RANDUTILS_H
#define RANDUTILS_H

#include <stdint.h>
#include <random>

/*!
 * \brief Returns sample from U(a,b)
 * \param r RNG
 * \param a a
 * \param b b
 */
template<typename T>
double urand(T &r, double a = 0, double b = 1) {
	std::uniform_real_distribution<double> uniform;
	double td = uniform(r);
	return (1.0 - td) * a + td * b;
}

/*!
 * \brief Returns sample from normal distribution
 * \param r RNG
 * \param m Mean
 * \param sigma Std
 */
template<typename T>
double nrand(T &r, double m = 0, double sigma = 1) {
	std::normal_distribution<double> d(m, sigma);
	return d(r);
}

/*!
 * \brief Returns sample from discrete distribution
 * \param r RNG
 * \param prob Probabilities
 * \param prob_len Number of probabilities
 * \param p P
 * \param s Scale
 */
template<typename T>
int rselect(T &r, const double * prob, int prob_len, double p = -1.0, double s = 1.0) {
	double x = p < 0 ? urand(r) : p, cumsum = 0.0;
	int curr = 0;
	while(prob[curr] == 0.0 || cumsum + prob[curr] / s < x) {
		cumsum += prob[curr] / s;
		++curr;
	}
	return curr;
}

/*!
 * \brief Returns sample from Bernoulli distribution
 * \param r RNG
 * \param prob Distribution param
 */
template<typename T>
bool rtest(T &r, double prob) {
	return urand(r) < prob;
}


#endif
