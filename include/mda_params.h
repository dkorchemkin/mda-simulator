#ifndef MDA_PARAMS_H
#define MDA_PARAMS_H

#include <cstdint>

//! Result of step
enum CopyResult {
	OK, //! OK step
	Replace, //! Replace step
	Skip,    //! Skip step
	Add,     //! Add step
	Invalid //! Invalid step
};
//! Limit type for strands TTL
enum class LimitType { 
	Off, //! TTL is unlimited
	Fixed, //! TTL is a fixed number
	Normal //! TTL is normally distributed
};

//! Struct for storing params
struct mda_params {
	//! Error probability [current char][result]
	double p_err[4][4]; // p_err[*,#] : P(#|*)  * = char, # = {O, R, S, A}
	//! EOL probability [result][current char source][appended char]
	double p_eol[4][4][4]; // p_eol[#,*,+] : P_eol(#|*+)  *+ = char, # = {O, R, S, A}
	//! Insertion probabilities [current char][inserted char]
	double p_add[4][4]; // p_add[*,#] : P_add(#|*)  * = char, # = new char
	//! Replace probabilities [current char][replacement]
	double p_replace[4][4]; // p_replace[*,#] : P_add(#|*)  * = char, # = new char
	//! Primer length
	size_t 	primer_len;
	//! Primer number for first iteration (per 5000000)
	size_t primer_n;   	
	//! TTL limit (if fixed)
	int pos_limit = INT32_MAX;
	//! TTL limit mean (if normal)
	double pos_limit_m = 40000.0;
	//! TTL limit std (if normal)
	double pos_limit_sigma = 20000; 	
	//! TTL limit type
	LimitType limit_type = LimitType::Off;
	//! true if primers should be reused
	bool reusePrimers;
	//! true if primer count should be scaled
	bool scalePrimers;
	//! Constructor
	mda_params();
	//! Returns probability of OK for source char idx
	double p_ok_copy(int idx) { 
		return p_err[idx][OK] * (1.0 - p_eol[OK][idx][idx ^ 1]);
	}    
	//! Return probability of non-OK for source char idx
	double p_error_copy(int idx) {
		return 1.0 - p_err[idx][OK] + p_err[idx][OK] * p_eol[OK][idx][idx ^ 1];
	}
private:
	bool checkParams();
};

#endif
