#ifndef STREAMSELECTOR_H
#define STREAMSELECTOR_H

#include <random>
#include <algorithm>
#include <vector>

template<typename T, typename RNG, typename RNGSeed, bool fastForward>
class StreamSelector;

/*!
 * \brief Provides equal probability selection from stream
 *
 * This class implements equal probability selection from stream with fast-forward steps enabled (heap of pairs <T,double> is used)
 */
template<typename T, typename RNG, typename RNGSeed>
class StreamSelector<T,RNG,RNGSeed,true> {
public:
	/*!
	 * \brief StreamSelector constructor with inner RNGS
	 * \param allocated For memory profiling
	 * \param K K items to select
	 * \param threads Maximum possible thread number
	 * \param seed Seed for RNG initialization
	 */
	StreamSelector(size_t &allocated, size_t K, size_t threads = 1, size_t seed = 1) : data(new (std::nothrow) std::pair<double, T>*[threads]), result(nullptr), threads(threads), k(new (std::nothrow) size_t[threads]{0}), res_size(0), K(K), skip(new (std::nothrow) size_t[threads]{0}), skipped(new (std::nothrow) size_t[threads]{0}), rngs(static_cast<RNG*>(operator new[] (sizeof(RNG) * threads, std::nothrow))), uniform_rng(new std::uniform_real_distribution<double>[threads]), delete_rngs(true) {
		if(!(data && k && rngs && skip && skipped)) {
			freeMem();
			std::bad_alloc exc;
			throw exc;
		}


		allocated = 0;
		allocated += sizeof(data[0]);
		allocated += sizeof(k[0]);
		allocated += sizeof(skip[0]);
		allocated += sizeof(skipped[0]);
		allocated += sizeof(rngs[0]);
		allocated += sizeof(uniform_rng[0]);
		allocated *= threads;
		allocated += sizeof(*this);

		RNGSeed rng_seed(seed);
		for(size_t i = 0; i < threads; ++i) {
			data[i] = static_cast<std::pair<double, T>*>(operator new[] (sizeof(std::pair<double, T>) * (K + 1), std::nothrow));
			new (rngs + i) RNG(rng_seed());
			allocated += sizeof(data[0][0]) * (K + 1);
		}
		for(size_t i = threads; i > 0; --i)
			if(!data[i - 1]) {
				freeMem();
				std::bad_alloc exc;
				throw exc;
			}

	}
	/*!
	 * \brief StreamSelector constructor with provided RNGS
	 * \param allocated For memory profiling
	 * \param K K items to select
	 * \param threads Maximum possible thread number
	 * \param rngs RNGs
	 * \param urngs U(0,1)
	 */
	StreamSelector(size_t &allocated, size_t K, size_t threads = 1, RNG* rngs = 0, std::uniform_real_distribution<double> *urngs = 0) : data(new (std::nothrow) std::pair<double, T>*[threads]), result(nullptr), threads(threads), k(new (std::nothrow) size_t[threads]{0}), res_size(0), K(K), skip(new (std::nothrow) size_t[threads]{0}), skipped(new (std::nothrow) size_t[threads]{0}), rngs(rngs), uniform_rng(urngs), delete_rngs(false) {
		if(!(data && k && rngs && skip && skipped)) {
			freeMem();
			std::bad_alloc exc;
			throw exc;
		}


		allocated = 0;
		allocated += sizeof(data[0]);
		allocated += sizeof(k[0]);
		allocated += sizeof(skip[0]);
		allocated += sizeof(skipped[0]);
		allocated += sizeof(rngs[0]);
		allocated += sizeof(uniform_rng[0]);
		allocated *= threads;
		allocated += sizeof(*this);

		for(size_t i = 0; i < threads; ++i) {
			data[i] = static_cast<std::pair<double, T>*>(operator new[] (sizeof(std::pair<double, T>) * (K + 1), std::nothrow));
			allocated += sizeof(data[0][0]) * (K + 1);
		}
		for(size_t i = threads; i > 0; --i)
			if(!data[i - 1]) {
				freeMem();
				std::bad_alloc exc;
				throw exc;
			}

	}
	//! StreamSelector destructor
	~StreamSelector() {
		freeMem();
	}
	/*!
	 * \brief Clears all memory
	 *
	 * Note: you should not call any method after calling clear()
	 */
	void clear() {
		for(size_t t = 0; t < threads; t++) {
			for(size_t i = 1; i <= k[t]; ++i)
				data[t][i].~pair();
			k[t] = 0;
		}
		freeHeaps();
	}
	//!  \brief Combines each thread solution into a single one
	void reduce() {
		// combine all thread heaps
		for(size_t t = 1; t < threads; t++) {
			for(size_t j = 1; j <= k[t]; j++) {
				if(k[0] < K) {
					data[0][++k[0]] = { data[t][j].first, data[t][j].second };
					if(k[0] == K)
						for(size_t i = K / 2; i >= 1u; i--)
							prepare_heap(i, 0);
				} else {
					if(data[t][j].first > data[0][1].first)
						heap_replace_top(data[t][j].second, data[t][j].first, 0);
				}
				data[t][j].~pair();
			}
			k[t] = 0;
		}
	}
	/*!
	 * \brief Makes one step
	 * \param item Current item in stream
	 * \param thread Current thread id
	 */
	void step(const T &item, size_t thread = 0) {
		if(k[thread] >= K) {
			if(skipped[thread] >= skip[thread]) {
				double top = data[thread][1].first;
				double alpha = top + uniform_rng[thread](rngs[thread]) * (1.0 - top);
				heap_replace_top(item, alpha, thread);
				skip[thread] = advance(data[thread][1].first, rngs[thread], thread);
				skipped[thread] = 1;
			} else 
				skipped[thread]++;
		} else {
			double alpha = uniform_rng[thread](rngs[thread]);
			data[thread][++k[thread]] = { alpha, item };
			if(k[thread] == K) {
				for(size_t i = K / 2; i >= 1u; i--)
					prepare_heap(i, thread);
				skip[thread] = advance(data[thread][1].first, rngs[thread], thread);
				skipped[thread] = 1;
			}
		}
	}
	/*!
	 * \brief Makes one step and returns fast forward steps number
	 * \param item Current item in stream
	 * \param thread Current thread id
	 * \return Number of items to skip
	 */
	size_t stepFastForward(const T& item, size_t thread = 0) {
		// RNG + heap processing + fast forward
		if(k[thread] >= K) {
			double top = data[thread][1].first;
			double alpha = top + uniform_rng[thread](rngs[thread]) * (1.0 - top);
			heap_replace_top(item, alpha, thread);
			return advance(data[thread][1].first, rngs[thread], thread);
		} else {
			data[thread][++k[thread]] = { uniform_rng[thread](rngs[thread]), item };
			if(k[thread] == K) {
				for(size_t i = K / 2; i >= 1u; i--)
					prepare_heap(i, thread);
				double top = data[thread][1].first;
				return advance(top, rngs[thread], thread);
			}
			return 1;
		}
	}
	/*!
	 * \brief Makes result from first thread data
	 * \param size Result size
	 * \param recreate true if result needs to be recreated
	 * \return Pointer to result array
	 */
	const T* fetchResult(size_t &size, bool recreate = true) {
		if(!recreate && result) {
			size = res_size;
			return result;
		}
		if(result)
			for(size_t i = 0; i < res_size; ++i)
				result[i].~T();
		else
			result = static_cast<T*>(operator new[] (K * sizeof(T)));
		for(size_t i = 0; i < k[0]; ++i)
			new (result + i) T(data[0][i + 1].second);
		size = res_size = k[0];
		return result;
	}
	/*!
	 * \brief Copies result to ptr
	 * \param res Pointer where to copy
	 */
	void fetchResult(T* res) {
		for(size_t i = 0; i < k[0]; ++i)
			new (res + i) T(data[0][i + 1].second);
		return;
	}

	/*!
	 * \brief Copy constructor
	 * \param rhs RHS
	 */
	StreamSelector(const StreamSelector<T, RNG, RNGSeed,true> &rhs) :
		data(new (std::nothrow) std::pair<double, T>*[rhs.threads]),
		result(rhs.result),
		threads(rhs.threads),
		k(new (std::nothrow) size_t[threads]),
		res_size(rhs.res_size),
		K(rhs.K),
		skip(new (std::nothrow) size_t[threads]),
		skipped(new (std::nothrow) size_t[threads]),
		rngs(static_cast<RNG*>(operator new[] (sizeof(RNG) * rhs.threads, std::nothrow))),
		uniform_rng(new std::uniform_real_distribution<double>[rhs.threads]) {
	
		if(!(data && rngs && skipped && skip)) {
			freeMem();
			std::bad_alloc exc;
			throw exc;
		}

		for(size_t i = 0; i < threads; ++i) {
			data[i] = static_cast<std::pair<double, T>*>(operator new[] (sizeof(std::pair<double, T>) * (K + 1), std::nothrow));
			if(!data[i]) {
				freeMem();
				std::bad_alloc exc;
				throw exc;
			}
			std::copy(rhs.data[i] + 1, rhs.data[i] + 1 + rhs.k[i], data[i] + 1);
			k[i] = rhs.k[i];
			new (rngs + i) RNG(rhs.rngs[i]);
		}

		if(result != nullptr) {
			result = static_cast<T*>(operator new[] (sizeof(T) * K));
			std::copy(rhs.result, rhs.result + rhs.res_size, result);
		}
	}

	/*!
	 * \brief Assignment operator
	 * \param rhs RHS
	 */
	StreamSelector<T, RNG, RNGSeed,true>& operator=(StreamSelector<T, RNG, RNGSeed,true> rhs) {
		std::swap(rhs, *this);
		return *this;
	}
private:
	inline size_t parent(const size_t& i) const { return i / 2; };
	inline size_t left_child(const size_t& i) const { return i * 2; };
	inline size_t right_child(const size_t& i) const { return i * 2 + 1; };
	inline size_t advance(const double &top, RNG &rng, size_t thread) {
		return std::min((double)std::numeric_limits<size_t>::max(), std::floor(std::log(uniform_rng[thread](rng)) / std::log(top)) + 1.0);
	}

	inline void heap_replace_top(const T& item, const double &alpha, const size_t &thread = 0) {
		std::swap(data[thread][1], data[thread][K]);
		data[thread][K] = {alpha, item};
		size_t sink = 1;
		size_t l, r, m, p;
		while(true) {
			m = sink;
			l = left_child(sink);
			r = right_child(sink);
			if(l < K && data[thread][l].first < data[thread][sink].first)
				m = l;
			if(r < K && data[thread][r].first < data[thread][m].first)
				m = r;
			if(m == sink)
				break;
			std::swap(data[thread][sink], data[thread][m]);
			sink = m;
		}
		size_t swim = K;
		while(true) {
			p = parent(swim);
			if(!p || data[thread][p].first < data[thread][swim].first)
				break;
			std::swap(data[thread][p], data[thread][swim]);
			swim = p;
		}
	}

	inline void prepare_heap(const size_t &u, const size_t &thread) {
		size_t m = u, l = left_child(u), r = right_child(u);
		if(l <= K && data[thread][l].first < data[thread][u].first)
			m = l;
		if(r <= K && data[thread][r].first < data[thread][m].first)
			m = r;
		if(m != u) {
			std::swap(data[thread][m], data[thread][u]);
			prepare_heap(m, thread);
		}
	}


	void freeHeaps() {
		if(data) {
			for(size_t t = 0; t < threads; ++t) {
				for(size_t j = 1; j <= k[t]; ++j)
					data[t][j].~pair();
				operator delete[] (data[t]);
			}
			delete[] data;
		}
		data = nullptr;
		
		if(k) delete[] k;
		k = nullptr;
	}
	void freeResult() {
		if(result) {
			for(size_t i = 0; i < res_size; ++i)
				result[i].~T();
			operator delete[] (result);
		}
		result = nullptr;
		res_size = 0;
	}
	void freeMem() {
		freeHeaps();
		freeResult();
		if(delete_rngs) {
		if(rngs) {
			for(size_t i = 0; i < threads; ++i)
				rngs[i].~RNG();
			operator delete[] (rngs);
		}
		if(uniform_rng) {
			delete[] uniform_rng;
			uniform_rng = nullptr;
		}}
		if(skip) {
			delete[] skip;
			skip = nullptr;
		}
		if(skipped) {
			delete[] skipped;
			skipped = nullptr;
		}
		rngs = nullptr;
	}
	std::pair<double, T> **data;
	T *result;
	size_t threads, *k, res_size, K;
	size_t *skip, *skipped;
	RNG *rngs;
	std::uniform_real_distribution<double> *uniform_rng;
	bool delete_rngs;
};

/*!
 * \brief Provides equal probability selection from stream
 *
 * This class implements equal probability selection from stream with fast-forward steps disabled (array of T is used)
 */

template<typename T, typename RNG, typename RNGSeed>
class StreamSelector<T,RNG,RNGSeed,false> {
public:
	/*!
	 * \brief StreamSelector constructor with inner RNGS
	 * \param allocated For memory profiling
	 * \param K K items to select
	 * \param threads Maximum possible thread number
	 * \param seed Seed for RNG initialization
	 */
	StreamSelector(size_t &allocated, size_t K, size_t threads = 1, size_t seed = 1) : data(new (std::nothrow) T*[threads]), threads(threads), k(new (std::nothrow) size_t[threads]{0}), K(K), rngs(static_cast<RNG*>(operator new[] (sizeof(RNG) * threads, std::nothrow))), uniform_rng(new std::uniform_real_distribution<double>[threads]), delete_rngs(true), reduced(0) {
		if(!(data && k && rngs)) {
			freeMem();
			std::bad_alloc exc;
			throw exc;
		}


		allocated = 0;
		allocated += sizeof(data[0]);
		allocated += sizeof(k[0]);
		allocated += sizeof(rngs[0]);
		allocated += sizeof(uniform_rng[0]);
		allocated *= threads;
		allocated += sizeof(*this);

		RNGSeed rng_seed(seed);
		for(size_t i = 0; i < threads; ++i) {
			data[i] = static_cast<T*>(operator new[] (sizeof(T) * (K), std::nothrow));
			new (rngs + i) RNG(rng_seed());
			allocated += sizeof(data[0][0]) * (K);
		}
		for(size_t i = threads; i > 0; --i)
			if(!data[i - 1]) {
				freeMem();
				std::bad_alloc exc;
				throw exc;
			}

	}
	/*!
	 * \brief StreamSelector constructor with provided RNGS
	 * \param allocated For memory profiling
	 * \param K K items to select
	 * \param threads Maximum possible thread number
	 * \param rngs RNGs
	 * \param urngs U(0,1)
	 */
	StreamSelector(size_t &allocated, size_t K, size_t threads = 1, RNG* rngs = 0, std::uniform_real_distribution<double> *urngs = 0) : data(new (std::nothrow) T*[threads]), threads(threads), k(new (std::nothrow) size_t[threads]{0}), K(K), rngs(rngs), uniform_rng(urngs), delete_rngs(false), reduced(0) {
		if(!(data && k && rngs)) {
			freeMem();
			std::bad_alloc exc;
			throw exc;
		}


		allocated = 0;
		allocated += sizeof(data[0]);
		allocated += sizeof(k[0]);
		allocated += sizeof(rngs[0]);
		allocated += sizeof(uniform_rng[0]);
		allocated *= threads;
		allocated += sizeof(*this);

		for(size_t i = 0; i < threads; ++i) {
			data[i] = static_cast<T*>(operator new[] (sizeof(T) * (K), std::nothrow));
			allocated += sizeof(data[0][0]) * (K);
		}
		for(size_t i = threads; i > 0; --i)
			if(!data[i - 1]) {
				freeMem();
				std::bad_alloc exc;
				throw exc;
			}

	}
	//! StreamSelector destructor
	~StreamSelector() {
		freeMem();
	}
	//! Clears memory (skips reduced!)
	void clear() {
		for(size_t t = 0; t < threads; t++) {
			if(t == reduced)
				continue;
			for(size_t i = 0; i < k[t] && i < K; ++i)
				data[t][i].~T();
			operator delete[] (data[t]);
			data[t] = 0;
			k[t] = 0;
		}
	}
	//! Combines jobs from threads
	void reduce() {
		// combine all thread arrays
		reduced = 0;
		for(size_t t = 1; t < threads; ++t) {
			if(t == reduced)
				continue;
			if(k[t] < K)
				for(size_t i = 0; i < k[t]; ++i)
					step(data[t][i], reduced);
			else {
				if(k[reduced] < K) {
					for(size_t i = 0; i < k[reduced]; ++i)
						step(data[reduced][i], t);
					reduced = t;
				} else {
					double left = k[reduced];
					double right = k[t];
					double total = k[reduced] = left + right;
					for(size_t i = 0; i < K; ++i)
						if(uniform_rng[0](rngs[0]) * total > left)
							data[reduced][i] = data[t][i];
				}
			}
		}
	}
	/*!
	 * \brief Makes one selection step
	 * \param item Current item in stream
	 * \param thread Current thread idx
	 */
	void step(const T &item, size_t thread = 0) {
		if(k[thread] < K) {
			data[thread][k[thread]++] = item;
		} else {
			size_t from = ++k[thread];
			size_t idx = rngs[thread]() % from;
			if(idx < K)
				data[thread][idx] = item;
		}
	}

	/*!
	 * \brief Returns pointer to result
	 * \param size Output param for result size
	 * \param recreate Not used in this template specialization
	 */
	const T* fetchResult(size_t &size, bool recreate = true) {
		size = std::min(k[reduced], K);
		return data[reduced];
	}

	/*!
	 * \brief Copy constructor
	 * \param rhs RHS
	 */
	StreamSelector(const StreamSelector<T, RNG, RNGSeed,false> &rhs) :
		data(new (std::nothrow) T*[rhs.threads]),
		threads(rhs.threads),
		k(new (std::nothrow) size_t[threads]),
		K(rhs.K),
		rngs(static_cast<RNG*>(operator new[] (sizeof(RNG) * rhs.threads, std::nothrow))),
		uniform_rng(new std::uniform_real_distribution<double>[rhs.threads]), reduced(rhs.reduced) {
	
		if(!(data && rngs)) {
			freeMem();
			std::bad_alloc exc;
			throw exc;
		}

		for(size_t i = 0; i < threads; ++i) {
			data[i] = static_cast<T*>(operator new[] (sizeof(T) * (K), std::nothrow));
			if(!data[i]) {
				freeMem();
				std::bad_alloc exc;
				throw exc;
			}
			std::copy(rhs.data[i], rhs.data[i] + rhs.k[i], data[i]);
			k[i] = rhs.k[i];
			new (rngs + i) RNG(rhs.rngs[i]);
		}
	}

	/*!
	 * \brief Assignment operator
	 * \param rhs RHS
	 */
	StreamSelector<T, RNG, RNGSeed,false>& operator=(StreamSelector<T, RNG, RNGSeed,false> rhs) {
		std::swap(rhs, *this);
		return *this;
	}
	/*!
	 * \brief Frees result memory
	 * Note: you should not call any function after this
	 */
	void freeResult() {
		if(data && data[reduced]) {
			for(size_t i = 0; i < k[reduced] && i < K; ++i)
				data[reduced][i].~T();
			operator delete[] (data[reduced]);
			data[reduced] = nullptr;
		}
		if(data)
			delete[] data;
		data = nullptr;
		res_size = 0;
	}
private:
	void freeTemporary() {
		if(data) {
			for(size_t t = 0; t < threads; ++t) {
				if(!data[t])
					continue;
				for(size_t j = 0; j < k[t] && j < K; ++j)
					data[t][j].~T();
				operator delete[] (data[t]);
			}
			delete[] data;
		}
		data = nullptr;
		
		if(k) delete[] k;
		k = nullptr;
	}
	void freeMem() {
		freeTemporary();
		freeResult();
		if(delete_rngs) {
		if(rngs) {
			for(size_t i = 0; i < threads; ++i)
				rngs[i].~RNG();
			operator delete[] (rngs);
		}
		if(uniform_rng) {
			delete[] uniform_rng;
			uniform_rng = nullptr;
		}}
		rngs = nullptr;
	}
	T **data;
	size_t threads, *k, res_size, K;
	RNG *rngs;
	std::uniform_real_distribution<double> *uniform_rng;
	bool delete_rngs;
	size_t reduced;
};



#endif
