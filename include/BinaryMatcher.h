#ifndef BINARYMATCHER_H
#define BINARYMATCHER_H

#include <vector>
#include <string>
#include <queue>
#include <ostream>
#include <cassert>
#include <cstdint>
#include <cstring>
#include <iostream>

/*!
 * \brief Primer mapper
 *
 * Converts current state to primer index.
 *
 * Note: only short (\f$\leq 12\f$) primers are supported (table of \f$4^L\cdot 4\f$ bytes should fit memory; \f$L\f$ - primer length)
 */
class BinaryIndexer {
public:
	/*!
	 * \brief BinaryIndexer constructor
	 * \param primer_len Primer length
	 */
	BinaryIndexer(uint32_t primer_len) : primer_index(new int[1ULL << (2 * primer_len)]()), len(primer_len), indexed_len(0), mask((1ULL << (2 * len)) - 1ULL), status(0) { }

	//! Destructor
	~BinaryIndexer() { delete[] primer_index; }

	/*!
	 * \brief Prepares state->primer index map
	 *
	 * \param vs Vector of primer strings
	 */
	void prepare(std::vector<std::string> &vs) {
		uint64_t key;
		for (size_t i = 0; i < vs.size(); ++i) {
			key = 0;
			assert(len == vs[i].size());
			for (size_t j = 0; j < len; ++j)
				key = key << 2 | map(vs[i][j]);
			primer_index[key] = i + 1;
		}
	}

	//! \brief Cleans state->primer index map
	void clear() {
		memset(primer_index, 0, (1ULL << (2 * len)) * sizeof(int));
	}

	/*! 
	 * \brief Makes one-step state update
	 * \param c Current DNA nucleotide
	 * \return -1 if current state does not correspond to any of primers, primer index otherwise
	 */
	inline int step(const char &c){
		int nres = c;
		status = status << 2 | nres;
		status &= mask;
		indexed_len += 1;
		if (indexed_len >= len && primer_index[status] > 0)
			return primer_index[status] - 1;
		return -1;
	};

	//! \brief Resets current state
	inline void reset() {
		status = indexed_len = 0;
	}

private:
	BinaryIndexer(const BinaryIndexer&) = delete;
	BinaryIndexer& operator=(const BinaryIndexer&) = delete;
	int* primer_index;
	uint32_t len;
	uint32_t indexed_len;
	uint64_t mask;
	uint64_t status;
	inline static uint64_t map(const char &c) {
		return c;
	}
};

#endif
