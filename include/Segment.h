#ifndef SEGMENT_H
#define SEGMENT_H

#include <ostream>
#include <cstdint>
#include <iostream>
#include <cassert>

#include "DNAString.h"
#include "Strand.h"

//TODO: remove mess using some wrapper allowing "RTTI" from 32-bit "pointers"

//! Base class for strand segments
class Segment {
public:
	//! Segment constructor
	Segment(bool ss=true) : 
#ifndef PTR32
		ss(ss),
#endif
		next(0)
			, base(0) {}
#ifndef PTR32
	//! Returns segment length
	inline uint32_t length() const { return ss ? len_ : 1; }
#else
	//! Returns segment length
	static inline uint32_t length(seg_ptr ptr);
#endif
	//! Returns nucleotide at position idx
	inline const char& operator[](const uint32_t& idx) const { 
#ifndef NDEBUG
#ifndef PTR32
		assert(idx < length()); 
#endif
#endif
		return base[idx]; 
	}
	/*! Outputs Segment to ouput stream
	 * \param os Output stream
	 * \param s Segment
	 */
	friend std::ostream& operator<<(std::ostream& os, const Segment& s);
	//! Next segment in list
	seg_ptr next
		;
protected:
	//! Lenght field for StringSegment and char field for ErrorSegment
	uint32_t len_;
	//! Base pointer
	const char* base;
#ifndef PTR32
	//! RTTI-like placeholder
	bool ss;
#endif
private:
	void print(std::ostream& os) const { 
#ifndef PTR32
		os << "["; for(size_t i = 0; i < length(); ++i) os << base[i]; os << "]"; 
#else
		os << "[SEGMENT OUTPUT DISABLED DUE TO PTR32]";
#endif
	}
};

#ifdef ONE_DNA_STRING
extern DNAString *dna_string;
#endif

//! Class for copies of source DNA substrings
class StringSegment : public Segment {
public:
	/*!
	 * \brief StringSegment constructor
	 * \param dna_string Source DNAString 
	 * \param begin Begin position
	 * \param end End position
	 * \param complementary Complimentarity flag
	 */
	StringSegment(DNAString* const& dna_string, const uint32_t& begin, const uint32_t& end, const uint32_t& complementary);
	/*!
	 * \brief Constructs complementary StringSegment
	 * \param pos Position in segment
	 * \param ss Pre-allocated StringSegment 
	 */
	void complementary_str(uint32_t pos, StringSegment* ss) const;
	/*!
	 * \brief Constructs complementary StringSegment
	 * \param pos Position in segment
	 */
	Segment* complementary_str(uint32_t pos) const;
	//! \brief Constructs complementary StringSegment
	Segment* complementary() const { return complementary_str(0); }
	//! \brief Constructs complementary StringSegment
	void complementary(StringSegment* ss) const { complementary_str(0, ss); }

	/*!
	 * \brief Elongates segment
	 * \param cnt Size increment
	 */
	void elongate(std::uint32_t cnt = 1);
	/*!
	 * \brief Elongates segment in reverse direction
	 * \param cnt Size increment
	 */
	void elongate_reverse(std::uint32_t cnt = 1);
	/*!
	 * \brief Simulates successive OKs on segment
	 * \param pos_from Starting position in segment
	 * \param p U(0,1)
	 * \param next_ok Output param true if next step should be OK
	 * \return Number of successive OKs
	 */
	inline size_t succ_copy(size_t pos_from, double p, bool &next_ok) {
		return dna_string->succ_copy(begin + pos_from, begin + len_ - 1, comp, p, next_ok);
	}
	/*!
	 * \brief Updates coverage counter in DNAString
	 * \param oldBegin Old begin pos
	 * \param newBegin New begin pos
	 */
	void changeBond(int oldBegin, int newBegin) {
		dna_string->cnt[comp][begin + oldBegin]--;
		dna_string->cnt[comp][begin + newBegin]++;
	}
	/*!
	 * \brief Initiates bond in DNAString coverage counter
	 * \param beginIdx Begin pos
	 */
	void beginBond(int beginIdx) {
		dna_string->cnt[comp][begin + beginIdx]++;
	}
	/*!
	 * \brief Finalizes bond in DNAString coverage counter
	 * \param endIdx End pos
	 */
	void endBond(int endIdx) {
		dna_string->cnt[comp][begin + endIdx + 1]--;
	}
private:
#ifndef ONE_DNA_STRING
	DNAString * dna_string;
#endif
	uint32_t comp;
	uint32_t begin;
	void print(std::ostream& os) const;

};


//! Class for storing erroneous segments
class ErrorSegment : public Segment {
public:
	/*!
	 * \brief ErrorSegment constructor
	 * \param error Error nucleotide
	 */
	ErrorSegment(const char& error);
	//! Returns ErrorSegment length;
	uint32_t length() const;
	//! Creates complementary ErrorSegment
	Segment* complementary() const;
	/*!
	 * \brief Creates complementary ErrorSegment using placement new
	 */
	void complementary(ErrorSegment *es) const;
private:
	void print(std::ostream& os) const;
};

#ifdef PTR32
/*!
 * \brief Returns length of segment by ID
 * \param ptr id
 */
uint32_t Segment::length(seg_ptr ptr) {
	StringSegment* ss = ss32_allocator.convert(ptr);
	if(!ss) {
#ifndef NDEBUG
		ErrorSegment* es = es32_allocator.convert(ptr);
		assert(es);
#endif
		return 1;
	}
	return ss->len_;
}
#endif

#endif
