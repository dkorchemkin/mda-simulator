#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <algorithm>
#include <cassert>
#include <iostream>

namespace utils {
	//! Checks if nucleotide value is in valid range
	inline bool check(const char& c) {
		return c >= 0 && c < 4;
	}
	//! Returns complementary code
	inline char complementary(const char& c) {
#ifndef NDEBUG
		if(!check(c))
			std::cout << "F:" << c << std::endl;
		assert(check(c));
#endif
		return c ^ 1;
	}
	//! Transforms string to binary
	inline void string_transform(std::string& forward, std::string& reverse) {
		reverse.resize(forward.size());
		std::transform(forward.begin(), forward.end(), forward.begin(), ::tolower);
		std::transform(forward.begin(), forward.end(), forward.begin(), [](char c) { return (char)(c != 'a' ? c != 't' ? c != 'g' ? 3 : 2 : 1 : 0);  });
		std::transform(forward.rbegin(), forward.rend(), reverse.begin(), complementary);
#ifndef NDEBUG
		assert(reverse.size() == forward.size());
		for(size_t i = 0, j = forward.size() - 1; i < forward.size(); ++i, --j)
			assert(check(forward[i]) && check(reverse[j]) && forward[i] == complementary(reverse[j]));
#endif
	}
}
#endif
