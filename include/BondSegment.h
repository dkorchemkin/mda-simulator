#ifndef BONDSEGMENT_H
#define BONDSEGMENT_H
#include <cstdint>

#include "Strand.h"
#include "buildconfig.h"

/*!
 * \brief Struct representing bond
 *
 * BondSegment represents continuous segment of bonds between two strands
 * It has two BondContexts representing entry in linked list of bonds of each strand
 */
class BondSegment {
public:
	//! \brief Entry in linked list of bonds
	struct BondContext {
		//! Complementary strand
		s_ptr_store other; 

		//! Bond segment begin
		std::uint32_t begin;		
		//! Bond segment end
		std::uint32_t end; 
		
		//! Pointer to next BondSegment
		ls_ptr_store next; 		
		//! Pointer to previous BondSegment
		ls_ptr_store prev;		
		
		/*!
		 * \brief BondContext constructor
		 * \param other Complementary strand
		 * \param begin Bond segment begin
		 * \param end Bond segment end
		 * \param next Pointer to next BondSegment
		 * \param prev Pointer to previous BondSegment
		 */
		BondContext(s_ptr_store other, std::uint32_t begin, std::uint32_t end, ls_ptr_store next = 0, ls_ptr_store prev = 0);
	};
	/*!
	 * \brief BondSegment constructor
	 * \param A First strand
	 * \param B Second strand
	 * \param begin_A Bond segment begin on the first strand
	 * \param end_A Bond segment end on the first strand
	 * \param begin_B Bond segment begin on the second strand
	 * \param end_B Bond segment end on the second strand
	 */
	BondSegment(s_ptr_store A, s_ptr_store B, std::uint32_t begin_A, std::uint32_t end_A, std::uint32_t begin_B, std::uint32_t end_B);
	/*!
	 * \brief Returns BondContext valid for strand
	 * \param ctx Strand
	 * \return BondContext for strand ctx
	 */
	BondContext& context(s_ptr_store ctx);
	//! \brief Returns Bond segment length
	std::uint32_t length();
	/*!
	 * \brief Removes BondSegment from linked lists
	 * \param idx Index of BondSegment (for 32-bit "pointers" implementation)
	 */
	void del(ls_ptr_store idx);
	/*!
	 * \brief Outputs BondSegment
	 * \param os Output stream
	 * \param ctx Context to output
	 */
	void print(std::ostream& os, Strand * ctx);
private:
	BondContext A, B;
};

#endif
