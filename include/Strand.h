#ifndef STRAND_H
#define STRAND_H

#include <ostream>
#include <cstdint>
#include <vector>
#include <deque>
#include <list>

#include "buildconfig.h"

//! Enum for Strand events
enum class SEvent : char {
	OK, //! Single OK event
	OK_BURST, //! Multiple OK events
	REPLACE, //! Replace event
	SKIP, //! Skip event
	ADD,  //! Add event
	EOL //! EOL
};

//! Struct for storing Strand events
struct StrandEvent {
	//! Source segment
	seg_ptr src;
	//! Event begin
	uint32_t begin;
	//! Event end
	uint32_t end;
	//! Pos on source strand
	uint32_t strand_pos; 	
	//! Event type
	SEvent evt_type; 
	//! Replacement/insertion nucleotide
	char c;
};

//! Struct for storing info about new strands
struct NS_info {
	uint32_t//! Last processed event's pos (on Strand) 
		last_pos, //! Last processed event's time
		last_time, 		//! Next event to be processed time
		next_time, //! Next event to be processed pos (on Strand)

		next_pos,  	 //! Pos in Segment
		seg_pos,  	//! Event duration
		event_time,		//! Steps from initial moment
		steps = 0; 		
	//! Valid overtake event timestamp
	uint32_t valid_overtake = ~0u; 		
	//! Distance to next strand
	uint32_t dist_next, 	 
			 //! Start position
			 start_pos,				 
			 //! TTL limit
			 eol_limit = INT32_MAX;
	 //! True if strand is dead
	bool eol = false;
	//! Parent strand
	s_ptr_store parent;  
	//! Next new strand	
	s_ptr_store next_new;
	//! Previous new strand
	s_ptr_store prev_new; 
	//! Current segment on parent strand 	
	seg_ptr curr_segment;
	 //! List for events	
	 std::list<StrandEvent> strand_events;

	/*!
	 * \brief NS_info constructor
	 * \param parent Parent strand
	 * \param last_pos Last event position
	 * \param curr_segment Segment on parent strand
	 * \param seg_pos  Position in segment
	 * \param t Event duration
	 */
	NS_info(s_ptr_store parent, int32_t last_pos, seg_ptr curr_segment, int32_t seg_pos, int32_t t = 10);
};

/*!
 * \brief Stores information about strand
 * Strand stores information about strand created during MDA
 */
class Strand {
	friend class MDAModel;
	friend class BondSegment;
public:
	/*!
	 * \brief Strand constructor
	 * \param init Segment to start from
	 * \param cyclic true if strand should be cyclic
	 */
	Strand(seg_ptr init = 0, bool cyclic = false);
	//! \brief Strand destructor
	~Strand();
	//! \brief Returns strand length
	uint32_t length() const;
	//! \brief Returns strand cyclicity
	bool isCyclic() const;
	/*!
	 * \brief Outputs Strand to output stream
	 * \param os Output stream
	 * \param st Strand
	 */
	friend std::ostream& operator<<(std::ostream& os, Strand& st);
private:
	uint32_t len;
	bool cyclic;
	seg_ptr segments;
	ls_ptr_store links;
	s_ptr_store last_ns_evt;
	ns_ptr_store ns;
};

#endif
