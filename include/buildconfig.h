#ifndef BUILD_CONFIG
#define BUILD_CONFIG

#include <cstdio>

static const size_t threads = 4;
static const size_t threads_ps = 4;
static const size_t min_thread_limit = 2;
static const size_t mem_limit = 1024llu * 1024llu * 1024llu * 2llu;

class Segment;
class StringSegment;
class ErrorSegment;
class BondSegment;
class Strand;
class NS_info;

#ifdef PTR32
#ifndef BULKALLOC
#error Build is not possible since bulk allocator is not enabled
#endif
#include "BlockAllocator.h"
#endif

template<typename T, typename I, typename C, C&c>
class PtrStoreWrapper;

/*!
 * \brief PTR32 pointer wrapper
 *
 * This class is used for replacing pointers with 32-bit indicies in BlockAllocator 's.
 */
template<typename T, typename I, typename C, C &c>
class PtrWrapper {
	friend class PtrStoreWrapper<T,I,C,c>;
public: 
	/*!
	 * \brief Constructor
	 * \param idx Index in BlockAllocator
	 */
	PtrWrapper(const I idx = 0) : idx(idx), ptr(nullptr) {
	}
	/*!
	 * \brief Constructor from PtrStoreWrapper
	 * \param psw PtrStoreWrapper
	 */
	PtrWrapper(const PtrStoreWrapper<T,I,C,c> &psw) : idx(psw.idx), ptr(nullptr) {}

	/*!
	 * \brief Conversion to pointer
	 */
	T& operator*() const {
		if(idx && !ptr)
			ptr = c.convert(idx);
		return *ptr;
	}
	/*!
	 * \brief Dereference operator
	 */
	T* operator->() const {
		if(idx && !ptr)
			ptr = c.convert(idx);
		return ptr;
	}
	//! Operator returning index
	operator I() const {
		return idx;
	}
	//! Cast to storage class
	operator PtrStoreWrapper<T,I,C,c>() const {
		return PtrStoreWrapper<T,I,C,c>(idx);
	}
	//! Equality operator
	friend bool operator==(const PtrWrapper<T,I,C,c> &lhs, const PtrWrapper<T,I,C,c> &rhs) {
		return lhs.idx == rhs.idx;
	}
	//! Equality operator
	friend bool operator==(const PtrWrapper<T,I,C,c> &lhs, const PtrStoreWrapper<T,I,C,c> &rhs) {
		return lhs.idx == rhs.idx;
	}
	//! Equality operator
	friend bool operator==(const PtrStoreWrapper<T,I,C,c> &rhs, const PtrWrapper<T,I,C,c> &lhs) {
		return lhs.idx == rhs.idx;
	}
	//! index
	I idx;
	//! Pointer
	mutable T* ptr;
};

/*!
 * \brief PTR32 storage pointer wrapper
 *
 * This class is used for replacing pointers with 32-bit indicies in BlockAllocator 's; this one is stored
 */
template<typename T, typename I, typename C, C&c>
class PtrStoreWrapper {
	friend class PtrWrapper<T,I,C,c>;
public:
	/*!
	 * \brief Constructor
	 * \param idx Index in BlockAllocator
	 */
	PtrStoreWrapper(const I idx = 0) : idx(idx) {}
	//! Operaror returning index
	operator I() const {
		return idx;
	}
	//! Dereference operator
	T& operator*() const {
		return *PtrWrapper<T,I,C,c>(idx);
	}
	//! Member operator
	T* operator->() const {
		return PtrWrapper<T,I, C, c>(idx).operator->();
	}
	//! Equality operator
	friend bool operator==(const PtrStoreWrapper<T,I,C,c> &lhs, const PtrStoreWrapper<T,I,C,c> &rhs) {
		return lhs.idx == rhs.idx;
	}
	//! index
	I idx;
};



#ifdef PTR32
typedef std::uint32_t seg_ptr;
extern BlockAllocator<threads, StringSegment, true, true, false, 2> ss32_allocator;
extern BlockAllocator<threads, ErrorSegment, true, true, false, 2> es32_allocator;

extern BlockAllocator<1, BondSegment, true, true, true, 1> ls32_allocator;
typedef std::uint32_t ls_ptr_index;
typedef PtrStoreWrapper<BondSegment, ls_ptr_index, decltype(ls32_allocator), ls32_allocator> ls_ptr_store;
typedef PtrWrapper<BondSegment, ls_ptr_index, decltype(ls32_allocator), ls32_allocator> ls_ptr_use;
static_assert(sizeof(ls_ptr_store) == sizeof(std::uint32_t), "Size mismatch");

extern BlockAllocator<threads, Strand, true, true, false, 1> s32_allocator;
typedef std::uint32_t s_ptr_index;
typedef PtrStoreWrapper<Strand, s_ptr_index, decltype(s32_allocator), s32_allocator> s_ptr_store;
typedef PtrWrapper<Strand, s_ptr_index, decltype(s32_allocator), s32_allocator> s_ptr_use;
static_assert(sizeof(s_ptr_store) == sizeof(std::uint32_t), "Size mismatch");

extern BlockAllocator<threads, NS_info, false, true, false, 1> ns32_allocator;
typedef std::uint32_t ns_ptr_index;
typedef PtrStoreWrapper<NS_info, ns_ptr_index, decltype(ns32_allocator), ns32_allocator> ns_ptr_store;
typedef PtrWrapper<NS_info, ns_ptr_index, decltype(ns32_allocator), ns32_allocator> ns_ptr_use;
static_assert(sizeof(ns_ptr_store) == sizeof(std::uint32_t), "Size mismatch");
#else
typedef Segment* seg_ptr;
typedef BondSegment* ls_ptr_store;
typedef BondSegment* ls_ptr_use;
typedef Strand* s_ptr_store;
typedef Strand* s_ptr_use;
typedef NS_info* ns_ptr_store;
typedef NS_info* ns_ptr_use;
#endif

bool getSeg(seg_ptr, Segment* &res);


#endif
