#ifndef TIMER_H
#define TIMER_H

#include <ctime>
#include <chrono>
#include <vector>
#include <string>
#include <ostream>
#include <iostream>

//! \brief Basic timing class
class Timer {
public:
	//! \brief Timer constructor
	Timer() : last_clock(std::chrono::high_resolution_clock::now()) {}
	/*!
	 * \brief Outputs report to output stream
	 * \param os Output stream
	 */
	void printReport(std::ostream& os) {
		os << "--------\nTimings\n--------\n";
        std::chrono::nanoseconds total (0);
		for(auto it = events.begin(); it != events.end(); ++it) {
			os.width(20);
			os << it->first;
			os.width(20);
			os << it->second.count() << std::endl;
			total += it->second;
		}
		os << "--------" << std::endl;
		os.width(20);
		os << "Total:";
		os.width(20);
		os << total.count() << "\n--------\n";
	}
	//! Starts timer
	void startTick() {
		last_clock = std::chrono::high_resolution_clock::now();
	}
	/*! Stops timer on event
	 * \param evt Event description
	 */
	void stopTick(std::string evt) {
		events.push_back(std::make_pair(evt, std::chrono::high_resolution_clock::now()- last_clock));
		std::cout << events.rbegin()->first << " : " << events.rbegin()->second.count() << std::endl;
	}
	//! Resets timer
	void rewind() {
		events.clear();
	}
private:
    std::chrono::high_resolution_clock::time_point last_clock;
	std::vector<std::pair<std::string, std::chrono::nanoseconds> > events;
};

#endif
