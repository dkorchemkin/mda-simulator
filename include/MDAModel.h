#ifndef MODEL_H
#define MODEL_H

#include <vector>
#include <queue>
#include <deque>
#include <set>
#include <random>

#include "Strand.h"
#include "DNAString.h"
#include "BondSegment.h"
#include "Timer.h"
#include "mda_params.h"
#include "Segment.h"
#include "BlockAllocator.h"

#ifdef PTR32
#define ss_allocator ss32_allocator
#define es_allocator es32_allocator
#define ls_allocator ls32_allocator
#define s_allocator s32_allocator
#define ns_allocator ns32_allocator
#endif

/*!
 * \brief Main simulation class
 */
class MDAModel{
public:
	//! Enum for possible event outcome
	enum class EvtType { 
		//! Assertion event (not used)
		ASSERT, 
		//! All outcomes are allowed
		ALLOW_ALL, 
		//! Only error outcomes
		ALLOW_ERR, 
		//! Overtake event
		OVERTAKE 
	};
	//! Struct for storing event info
	struct Evt {
		//! Time of event
		size_t t;
		//! Creation timestamp
		size_t timestamp;
		//! Event type
		EvtType type;
		//! New strand
		s_ptr_store ns;
		/*!
		 * \brief Evt constructor
		 * \param ns New strand
		 * \param t Event time
		 * \param timestamp Event timestamp
		 * \param type Event type
		 */
		Evt(s_ptr_store ns, size_t t, size_t timestamp = 0, EvtType type = EvtType::ALLOW_ALL) 
			: t(t), timestamp(timestamp), type(type), ns(ns) {}
		/*!
		 * \brief Event comparator (for usage in priority queue)
		 * \param e RHS
		 */
		bool operator<(const Evt &e) const {
			return t != e.t ? t > e.t : type > e.type;
		}
	};
	typedef std::mt19937_64 Rng;
	//! Destructor
	~MDAModel();
	/*!
	 * \brief MDAModel constructor
	 * \param params Simulation params
	 * \param logp Log stream pointer
	 */
	MDAModel(mda_params params = mda_params(), std::ostream *logp = &std::cerr);
	//! Makes one step
	void step();
	/*!
	 * \brief Add DNAString to model
	 * \param s Source string
	 * \param cyclic true if new strand should be cyclic
	 */
	void addStrand(const std::string& s, bool cyclic = true);
	/*!
	 * \brief Computes coverage and writes coverage data to file
	 * \param output_file File name
	 */
	double computeCoverage(const std::string& output_file = "");
private:
	void processStrand(size_t strand, size_t thread, size_t &max_len, size_t &min_len, size_t &avg_len, size_t &cnt);
	void prepareNS(size_t strand, size_t thread, std::priority_queue<Evt> *&events, std::deque<s_ptr_store> &ns);
	void simulateNS(size_t strand, size_t thread, std::priority_queue<Evt> *&events);
	void computeBonds(size_t strand, size_t thread);
	void computeStrands(size_t strand, size_t thread, std::deque<s_ptr_store> &ns);
	void finalizeStrands(size_t strand, size_t thread, std::deque<s_ptr_store> &ns, size_t &max_len, size_t &min_len, size_t &avg_len, size_t &cnt);

	// Generate primers for the step
	void generatePrimers();
	// Find primers in free places on existing strands
	void findPrimers();
	// Select allowed number of primers
	void selectPrimers();
	void removeBadPrimers(size_t strand, size_t thread);

	void consistencyCheck();
	// Compute delete lists & new strand segments
	void checkBonds(s_ptr_store parent);
	// compute delete set
	void prepareDeleteSet(std::vector<uint32_t> &begins, std::vector<uint32_t>& ends, s_ptr_store parent);
	// delete from old link list & recalc coverage
	void deleteSet(std::vector<uint32_t> &begins, std::vector<uint32_t> &ends, s_ptr_store parent);
	// prepare new link list & recalc coverage
	void prepareNewBonds(s_ptr_store parent, ls_ptr_use &new_links);
	// merge new link list into cleared old
	void mergeBonds(s_ptr_store parent, ls_ptr_use new_links);
	// distance to next at time (dist + prev_steps + prev_nsteps - next_steps 
	int distToNext(s_ptr_store curr, uint32_t time, bool bs = false);
	int stepsUpdate(s_ptr_store s, uint32_t time);

	// process event
	CopyResult modelAllowAll(s_ptr_store strand, int char_index, StrandEvent& strand_event, seg_ptr curr_segment, uint32_t seg_pos, uint32_t time, uint32_t &new_pos, bool &eol, int &copied, bool &next_err, size_t thread = 0);
	CopyResult modelOvertake(s_ptr_store strand, int char_index, StrandEvent& strand_event, seg_ptr curr_segment, uint32_t seg_pos, uint32_t time, uint32_t &new_pos, bool &eol, int &copied, size_t thread = 0);
	CopyResult modelError   (s_ptr_store strand, int char_index, StrandEvent& strand_event, seg_ptr curr_segment, uint32_t seg_pos, uint32_t time, uint32_t &new_pos, bool &eol, int &copied, size_t thread = 0);

	// overtakes
	bool calculateOvertake(s_ptr_store A, s_ptr_store b, int curr_time, uint32_t& overtake_time);
	void updateOvertakes(s_ptr_store A, int time, std::priority_queue<Evt> *events = nullptr);
	bool reusePrimers, scalePrimers;
	mda_params params;
	int output, debug;
	std::ostream *o_log_p;

	std::deque<s_ptr_store> strands;
	std::deque<DNAString*> strings;
	
	Timer timer;

	std::vector<std::string> primers;
	std::deque<std::uint32_t> primer_selection;
	std::deque<std::uint32_t> primers_selection_cnt;
	std::deque<uint32_t *> strand_pos;
	std::deque<size_t> nss_cnt;

#ifndef PTR32
	BlockAllocator<threads, StringSegment> ss_allocator;
	BlockAllocator<threads, ErrorSegment> es_allocator;
	BlockAllocator<threads, NS_info, false> ns_allocator;
	BlockAllocator<threads, Strand, false> s_allocator;
	BlockAllocator<1, BondSegment, true, true, true, 1> ls_allocator;
#endif

	size_t steps = 0;
	uint64_t sym_cnt = 0;
	uint64_t cnt_ok = 0, cnt_rep = 0, cnt_add = 0, scale = 1;
	uint64_t copied_ok = 0, copied_add = 0, copied_skip = 0, copied_replace = 0;
	Rng* rngs;
	std::uniform_real_distribution<double> *urngs;
	uint32_t *nss_mem = nullptr;
};

#endif
