#ifndef FASTAREADER_H
#define FASTAREADER_H

#include <fstream>
#include <string>

/*!
 * \brief FASTA parser
 */
class FastaReader {
	public:
		/*!
		 * \brief FastaReader constructor
		 * Constructs FastaReader for reading FASTA data from file
		 * \param filename Filename
		 */
		FastaReader(const char *filename);
		/*!
		 * \brief Parses FASTA
		 * Parses FASTA and returns DNA string
		 * \return DNA string
		 */
		std::string read();
	private:
		std::ifstream in;
};

#endif
